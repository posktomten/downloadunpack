#!/bin/bash

# Edit
QT6=6.8.0
QT5=5.15.16
EXECUTABLE=downloadunpack-prg
#EXTRA_LIB=./lib

[ -e ${EXECUTABLE}-*.AppImage ] && rm ${EXECUTABLE}-*.AppImage
[ -e ${EXECUTABLE}-*.AppImage-SHA256.txt ] && rm ${EXECUTABLE}-*.AppImage-SHA256.txt
[ -e AppDir ] && rm -R AppDir


echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Qt${QT5}" "Qt${QT6}" "Quit")

select opt in "${options[@]}"; do
    case $opt in
    "Qt${QT5}")
        export PATH=/opt/Qt/${QT5}/gcc_64/bin:$PATH
        export LD_LIBRARY_PATH=/opt/Qt/${QT5}/gcc_64/lib:$LD_LIBRARY_PATH
        break
        ;;
    "Qt${QT6}")
        export PATH=/opt/Qt/${QT6}/gcc_64/bin:$PATH
        export LD_LIBRARY_PATH=/opt/Qt/${QT6}/gcc_64/lib:$LD_LIBRARY_PATH
        break
        ;;
    "Quit")
        echo "Goodbye!"
        exit 0
        break

        ;;
    *) echo "Invalid option $REPLY"
    ;;
    esac
done


linuxdeploy --appdir AppDir \
-e ./${EXECUTABLE} \
-e ./openssl/openssl \
-d ./${EXECUTABLE}.desktop \
-l ./openssl/libssl.so \
-l ./openssl/libssl.so.3 \
-l ./openssl/libcrypto.so \
-l ./openssl/libcrypto.so.3 \
-i ./16x16/${EXECUTABLE}.png \
-i ./32x32/${EXECUTABLE}.png \
-i ./64x64/${EXECUTABLE}.png  \
-i ./128x128/${EXECUTABLE}.png \
-i ./256x256/${EXECUTABLE}.png \
-i ./512x512/${EXECUTABLE}.png \
--plugin qt

cp ./${EXECUTABLE}.png  ./AppDir/usr/bin/
rm -R ./AppDir/usr/translations
appimagetool AppDir

echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Copy" "Upload" "Quit")
select opt in "${options[@]}"; do
    case $opt in
    "Copy")
        echo "You chose choice $REPLY which is $opt"
        yes | cp -rf ${EXECUTABLE}-x86_64.AppImage ../
        break
        ;;
    "Upload")
        echo "You chose choice $REPLY which is $opt"
        yes | cp -rf ${EXECUTABLE}-x86_64.AppImage ../
        ../upload_appimage_GLIBC2.39.sh
        break
        ;;
    "Quit")
        break
        ;;
    *) echo "invalid option $REPLY"
    ;;
    esac
done
