//#  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//#
//#         downloadunpack
//#         Copyright (C) 2016 - 2024 Ingemar Ceicer
//#         https://gitlab.com/posktomten/downloadunpack
//#         programming@ceicer.com
//#
//#   This program is free software: you can redistribute it and/or modify
//#   it under the terms of the GNU General Public License as published by
//#   the Free Software Foundation, either version 3 of the License, or
//#   (at your option) any later version.
//#
//#   This program is distributed in the hope that it will be useful,
//#   but WITHOUT ANY WARRANTY; without even the implied warranty of
//#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//#   GNU General Public License for more details.
//#
//# <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFontDatabase>
#include <QSettings>
#include <QProcess>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include "downloadunpack.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    move(screen()->geometry().center() - frameGeometry().center());
//    this->setFixedSize(448, 120);
    QString fontPath = ":/fonts/Ubuntu-R.ttf";
    int fontId = QFontDatabase::addApplicationFont(fontPath);
    QIcon icon(":/images/download.png");
    this->setWindowIcon(icon);

    if(fontId != -1) {
        QFont font("Ubuntu", FONTSIZE);
        this->setFont(font);
        ui->actionClose->setFont(font);
        ui->actionTest_the_library_downloadunpack->setFont(font);
    }

    connect(ui->actionClose, &QAction::triggered, [this]() {
        close();
    });

//    QIcon icon(":/images/download.png");
    connect(ui->actionTest_the_library_downloadunpack, &QAction::triggered, []() {
#ifdef Q_OS_WINDOWS
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        DownloadUnpack *du = new DownloadUnpack(true);
#endif
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
        DownloadUnpack *du = new DownloadUnpack(false);
#endif
#endif
#ifdef Q_OS_LINUX
        DownloadUnpack *du = new DownloadUnpack;
#endif
        du->showNormal();
    });

    connect(ui->pbTest, &QPushButton::clicked, ui->actionTest_the_library_downloadunpack, &QAction::triggered);
    connect(ui->pbAbout, &QPushButton::clicked, [this]() {
        ui->teOut->clear();
        QString EXECUTE;
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
        settings.beginGroup("Path");
        QString stablepath = settings.value("stablepath", QStandardPaths::writableLocation(QStandardPaths::DownloadLocation)).toString();
        QString bleedingedgepath = settings.value("bleedingedgepath", QStandardPaths::writableLocation(QStandardPaths::DownloadLocation)).toString();
#ifdef Q_OS_LINUX
        // qDebug() << stablepath;
        // qDebug() << bleedingedgepath;

        if(ui->rbStable->isChecked()) {
            EXECUTE = stablepath + "/stable/svtplay-dl";
        } else {
            EXECUTE = bleedingedgepath + "/beta/svtplay-dl";
        }

#endif
#ifdef Q_OS_WINDOWS

        if(ui->rbStable->isChecked()) {
            EXECUTE = stablepath + "/stable/svtplay-dl.exe";
        } else {
            EXECUTE = bleedingedgepath + "/beta/svtplay-dl.exe";
        }

#endif
        auto *CommProcess = new QProcess(this);
        QString *instring = new QString;
        CommProcess->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess->start(EXECUTE, QStringList() << "--version");
        CommProcess->waitForFinished(-1);
        QTextStream stream2(CommProcess->readAllStandardOutput());
        QString line2 = stream2.readAll();
        *instring += "<b>" + line2 + "</b><br>";
        CommProcess->start(EXECUTE, QStringList() << "--help");
        CommProcess->waitForFinished(-1);
        QTextStream stream(CommProcess->readAllStandardOutput());
        QString line;
        // int i = 0;

//        line = stream.readAll();
//        *instring += line;
        while(!stream.atEnd()) {
            line = stream.readLine();
//            line = line.simplified();
            *instring += "<br>" + line;
            // i++;
        }

        ui->teOut->setText(*instring);
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}
