cmake_minimum_required(VERSION 3.16)
project(downloadunpack-prg VERSION 1.0 LANGUAGES C CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)

find_package(QT NAMES Qt5 Qt6 REQUIRED COMPONENTS Core)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core Widgets Gui Network)
find_package(Qt${QT_VERSION_MAJOR} OPTIONAL_COMPONENTS Core Widgets Gui Network)

SET(CMAKE_DEBUG_POSTFIX "d")

qt_standard_project_setup()

qt_add_executable(downloadunpack-prg WIN32 MACOSX_BUNDLE
    main.cpp
    mainwindow.cpp
    mainwindow.h
    mainwindow.ui
)
target_include_directories(downloadunpack-prg PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/../include
)

target_link_libraries(downloadunpack-prg PRIVATE
    Qt::Core
    Qt::Gui
    Qt::Network
)


# Resources:
set(resurs_resource_files
    "fonts/Ubuntu-R.ttf"
    "images/download.png"
    "images/exit.png"
)

qt_add_resources(downloadunpack-prg "resurs"
    PREFIX
        "/"
    FILES
        ${resurs_resource_files}
)

if((QT_VERSION_MAJOR GREATER 4))
    target_link_libraries(downloadunpack-prg PRIVATE
        Qt::Widgets
    )
endif()



if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    target_link_libraries(downloadunpack-prg PRIVATE

        ${CMAKE_CURRENT_SOURCE_DIR}/../lib6/libdownloadunpackd.a
    )
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Release")
    target_link_libraries(downloadunpack-prg PRIVATE

        ${CMAKE_CURRENT_SOURCE_DIR}/../lib6/libdownloadunpack.a
    )
endif()



# install(TARGETS downloadunpack-prg
#     BUNDLE DESTINATION .
#     RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
# )

# qt_generate_deploy_app_script(
    # TARGET downloadunpack-prg
    # FILENAME_VARIABLE deploy_script
    # NO_UNSUPPORTED_PLATFORM_ERROR
# )
# install(SCRIPT ${deploy_script})
