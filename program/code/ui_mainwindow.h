/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.10
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
// #include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionTest_the_library_downloadunpack;
    QAction *actionClose;
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *pbTest;
    QPushButton *pbAbout;
    QRadioButton *rbStable;
    QRadioButton *rbBleedingedge;
    QSpacerItem *horizontalSpacer;
    QTextEdit *teOut;
    QMenuBar *menubar;
    QMenu *menuTest;
    QMenu *menuFile;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(557, 503);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(0, 0));
        MainWindow->setMaximumSize(QSize(16777215, 16777215));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/image/download.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        actionTest_the_library_downloadunpack = new QAction(MainWindow);
        actionTest_the_library_downloadunpack->setObjectName(QString::fromUtf8("actionTest_the_library_downloadunpack"));
        actionClose = new QAction(MainWindow);
        actionClose->setObjectName(QString::fromUtf8("actionClose"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionClose->setIcon(icon1);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pbTest = new QPushButton(centralwidget);
        pbTest->setObjectName(QString::fromUtf8("pbTest"));

        horizontalLayout->addWidget(pbTest);

        pbAbout = new QPushButton(centralwidget);
        pbAbout->setObjectName(QString::fromUtf8("pbAbout"));

        horizontalLayout->addWidget(pbAbout);

        rbStable = new QRadioButton(centralwidget);
        rbStable->setObjectName(QString::fromUtf8("rbStable"));
        rbStable->setChecked(true);

        horizontalLayout->addWidget(rbStable);

        rbBleedingedge = new QRadioButton(centralwidget);
        rbBleedingedge->setObjectName(QString::fromUtf8("rbBleedingedge"));

        horizontalLayout->addWidget(rbBleedingedge);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);

        teOut = new QTextEdit(centralwidget);
        teOut->setObjectName(QString::fromUtf8("teOut"));

        verticalLayout->addWidget(teOut);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 557, 26));
        menuTest = new QMenu(menubar);
        menuTest->setObjectName(QString::fromUtf8("menuTest"));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuTest->menuAction());
        menuTest->addAction(actionTest_the_library_downloadunpack);
        menuFile->addAction(actionClose);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Test the library downloadunpack", nullptr));
        actionTest_the_library_downloadunpack->setText(QCoreApplication::translate("MainWindow", "Test the library downloadunpack", nullptr));
        actionClose->setText(QCoreApplication::translate("MainWindow", "Exit", nullptr));
        pbTest->setText(QCoreApplication::translate("MainWindow", "Test", nullptr));
        pbAbout->setText(QCoreApplication::translate("MainWindow", "About", nullptr));
        rbStable->setText(QCoreApplication::translate("MainWindow", "About stable", nullptr));
        rbBleedingedge->setText(QCoreApplication::translate("MainWindow", "About beta", nullptr));
        menuTest->setTitle(QCoreApplication::translate("MainWindow", "Test", nullptr));
        menuFile->setTitle(QCoreApplication::translate("MainWindow", "File", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
