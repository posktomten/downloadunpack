#  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#
#    		downloadunpack
#    		Copyright (C) 2016 - 2023 Ingemar Ceicer
#    		https://gitlab.com/posktomten/downloadunpack
#    		programming@ceicer.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
# <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>



QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = downloadunpack-prg
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

CODECFORSRC = UTF-8
# UI_DIR = ../code


equals(QT_MAJOR_VERSION, 5) {
DESTDIR="../build-executable5"

CONFIG (release, debug|release): LIBS += -L../lib5/ -ldownloadunpack # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -ldownloadunpackd # Debug

# LIBS += -L../lib5/ -lssl # Release
# LIBS += -L../lib5/ -lcrypto # Release

}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR="../build-executable6"

CONFIG (release, debug|release): LIBS += -L../lib6/ -ldownloadunpack # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -ldownloadunpackd # Debug

# LIBS += -L../lib5/ -lssl # Release
# LIBS += -L../lib5/ -lcrypto # Release

}


INCLUDEPATH += "../include"
DEPENDPATH += "../include"

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resurs.qrc
