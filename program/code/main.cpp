//#  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//#
//#         downloadunpack
//#         Copyright (C) 2016 - 2024 Ingemar Ceicer
//#         https://gitlab.com/posktomten/downloadunpack
//#         programming@ceicer.com
//#
//#   This program is free software: you can redistribute it and/or modify
//#   it under the terms of the GNU General Public License as published by
//#   the Free Software Foundation, either version 3 of the License, or
//#   (at your option) any later version.
//#
//#   This program is distributed in the hope that it will be useful,
//#   but WITHOUT ANY WARRANTY; without even the implied warranty of
//#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//#   GNU General Public License for more details.
//#
//# <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
