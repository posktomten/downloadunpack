//#  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//#
//#         downloadunpack
//#         Copyright (C) 2016 - 2024 Ingemar Ceicer
//#         https://gitlab.com/posktomten/downloadunpack
//#         programming@ceicer.com
//#
//#   This program is free software: you can redistribute it and/or modify
//#   it under the terms of the GNU General Public License as published by
//#   the Free Software Foundation, either version 3 of the License, or
//#   (at your option) any later version.
//#
//#   This program is distributed in the hope that it will be useful,
//#   but WITHOUT ANY WARRANTY; without even the implied warranty of
//#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//#   GNU General Public License for more details.
//#
//# <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScreen>
#include <QStandardPaths>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include "downloadunpack.h"
#include "downloadunpack_global.h"
#include "rename.h"
#include "rename_global.h"

// #ifdef Q_OS_LINUX
// #define FONTSIZE 12
// #endif
// #ifdef Q_OS_WINDOWS
// #define FONTSIZE 11
// #endif

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}

QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
