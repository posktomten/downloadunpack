
set "SOKVAG=C:\Users\posktomten\PROGRAMMERING\compiled_lib\"

set "VER5=5.15.16"
set "VER6=6.8.1"


mkdir %SOKVAG%lib_Qt-5.15.2_MinGW
mkdir %SOKVAG%lib_Qt-5.15.2_MSVC2022
mkdir %SOKVAG%lib_Qt-%VER5%_MinGW
mkdir %SOKVAG%lib_Qt-%VER5%_MinGW_32
mkdir %SOKVAG%lib_Qt-%VER5%_MSVC2022
mkdir %SOKVAG%lib_Qt-%VER5%_MSVC2022_32
mkdir %SOKVAG%lib_Qt-%VER6%_MinGW
mkdir %SOKVAG%lib_Qt-%VER6%_MinGW_LLVM
mkdir %SOKVAG%lib_Qt-%VER6%_MSVC2022
mkdir %SOKVAG%lib_Qt-%VER6%_MSVC2022_static


xcopy /y lib_Qt-5.15.2_MinGW\*.* %SOKVAG%lib_Qt-5.15.2_MinGW\
xcopy /y lib_Qt-5.15.2_MSVC2022\*.* %SOKVAG%lib_Qt-5.15.2_MSVC2022\
xcopy /y lib_Qt-%VER5%_MinGW\*.* %SOKVAG%lib_Qt-%VER5%_MinGW\
xcopy /y lib_Qt-%VER5%_MinGW_32\*.* %SOKVAG%lib_Qt-%VER5%_MinGW_32\
xcopy /y lib_Qt-%VER5%_MSVC2022\*.* %SOKVAG%lib_Qt-%VER5%_MSVC2022\
xcopy /y lib_Qt-%VER5%_MSVC2022_32\*.* %SOKVAG%lib_Qt-%VER5%_MSVC2022_32\
xcopy /y lib_Qt-%VER6%_MinGW\*.* %SOKVAG%lib_Qt-%VER6%_MinGW\
xcopy /y lib_Qt-%VER6%_MinGW_LLVM\*.* %SOKVAG%lib_Qt-%VER6%_MinGW_LLVM\
xcopy /y lib_Qt-%VER6%_MSVC2022\*.* %SOKVAG%lib_Qt-%VER6%_MSVC2022\
xcopy /y lib_Qt-%VER6%_static_MSVC2022\*.* %SOKVAG%lib_Qt-%VER6%_MSVC2022_static\