// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadunpack
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/downloadunpack
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QFileDialog>
#include <QSettings>
#include <QStandardPaths>
#include <QDesktopServices>

#include "downloadunpack.h"


QString DownloadUnpack::fileDialog()
{
    QString vald = ui->comboSvtplaydl->currentText();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
//    settings.setIniCodec("UTF-8");
    settings.beginGroup("Path");
    QString svtplay_dlPath = settings.value("svtplay_dlPath", QStandardPaths::writableLocation(QStandardPaths::DownloadLocation)).toString();
    settings.endGroup();
    /* filediaalog */
    QFileDialog filedialog(this);
    int x = this->x();
    int y = this->y();
    filedialog.setDirectory(svtplay_dlPath);
    filedialog.setLabelText(QFileDialog::Accept, tr("Choose"));
    filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
    filedialog.setWindowTitle(tr("Save ") + vald + tr(" in directory"));
    filedialog.setGeometry(x + 50, y + 50, 900, 550);
    filedialog.setFileMode(QFileDialog::Directory);//Folder name

    if(filedialog.exec() == 1) {
        QDir dir = filedialog.directory();
        QString sdir = dir.path();
        settings.beginGroup("Path");
        settings.setValue("svtplay_dlPath", sdir);
        settings.endGroup();
        return sdir;
    } else {
        return "not";
    }
}
void DownloadUnpack::openFolder()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
//    settings.setIniCodec("UTF-8");
    settings.beginGroup("Path");
    QString svtplay_dlPath = settings.value("svtplay_dlPath", QStandardPaths::writableLocation(QStandardPaths::DownloadLocation)).toString();
    settings.endGroup();
    QDesktopServices::openUrl(svtplay_dlPath);
}
