// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadunpack
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/downloadunpack
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#include <QProcess>
#include <QSettings>
#include <QFileInfo>
#include <QDir>
#include <QTimer>

#include "rename.h"
#include "downloadunpack.h"
#include "ui_downloadunpack.h"

void DownloadUnpack::download(bool downloadlatest, const QString & latest, const QString & path)
{
    selectedsvtplaydl = ui->comboSvtplaydl->currentText();
    ui->pteEdit->clear();
    QString *svtplay_dlPath = nullptr;

    if(downloadlatest) {
        selectedsvtplaydl = latest;
    }

    if(path.isEmpty()) {
        svtplay_dlPath = new QString(fileDialog());

        if(*svtplay_dlPath == "not") {
            return;
        }
    } else {
        svtplay_dlPath = new QString(path);
    }

    if(svtplay_dlPath->isEmpty()) {
        return;
    }

    ui->actionDownload->setDisabled(true);
    ui->actionDownloadLatest->setDisabled(true);
    ui->actionDownloadLatestStable->setDisabled(true);
    ui->actionDownloadToBleedingedge->setDisabled(true);
    ui->actionDownloadToStable->setDisabled(true);
    ui->actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setDisabled(true);
    ui->actionDownloadLatestStableToDirectoryStable->setDisabled(true);
    ui->actionSearch->setDisabled(true);
    ui->actionSearchLatestStable->setDisabled(true);
    ui->actionExit->setDisabled(true);
    QFileInfo fi(*svtplay_dlPath);

    if(!fi.isDir() && !fi.isWritable()) {
        ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n" + QDir::toNativeSeparators(*svtplay_dlPath) + " " + tr("Is not writable. Cancels"));
        return;
    }

// FRÅN
    ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + tr("Starting to download...\n"));
    QUrl url(currentOs() + selectedsvtplaydl);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    connect(reply, &QNetworkReply::errorOccurred, [this, reply](QNetworkReply::NetworkError error) {
        reply->disconnect();
        ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n" + networkErrorMessages(error));
        setAllEnabled();
        return;
    });

    connect(reply, &QIODevice::readyRead, [this]() {
        ui->progressBar->setValue(ui->progressBar->value() + 1);

        if(ui->progressBar->value() >= 100) {
            ui->progressBar->setValue(0);
        }
    });

    QObject::connect(
        reply, &QNetworkReply::finished,
    [reply, svtplay_dlPath, this]() {
        QByteArray bytes = reply->readAll(); // bytes
        QFile file(*svtplay_dlPath + "/" + selectedsvtplaydl);
        ui->progressBar->setValue(100);

        if(!(file.open(QIODevice::ReadWrite))) {
            ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n" + tr("Can not save") + " \"" + selectedsvtplaydl + "\" " + tr("to") + " \"" + QDir::toNativeSeparators(*svtplay_dlPath) + "\" " + tr("Check your file permissions and antivirus software."));
        } else  if(!(file.write(bytes.data(), bytes.size()) > 0)) {
            ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n" + tr("Can not save to") + " \"" + QDir::toNativeSeparators(*svtplay_dlPath) + "\" " + tr("Check your file permissions and antivirus software."));
        } else {
            ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n\"" + selectedsvtplaydl + "\" " + tr("is downloaded to") + " \"" + QDir::toNativeSeparators(*svtplay_dlPath) + "\".");
        }

        file.close();
        ui->progressBar->setValue(0);
// TILL
        // TRY TO UNZIP
        QProcess *process = new QProcess;
        process->setWorkingDirectory(*svtplay_dlPath);
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        process->setProcessEnvironment(env);
        connect(process, &QProcess::started,
        [this]() {
            ui->progressBar->setValue(0);
            ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n\n" + tr("Starting to decompress") + " \"" + selectedsvtplaydl + "\"...\n");
            timer = new QTimer(this);
            connect(timer, &QTimer::timeout, this, &DownloadUnpack::klocka);
            timer->start(200);
        });

        // Display output
        connect(process, &QProcess::readyReadStandardError,
        [process, this]() {
            QString result(process->readAllStandardError());
            ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + result);
        });

        connect(process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        [ = ](int exitCode) {
            ui->progressBar->setValue(0);
            delete timer;

            if(exitCode == 0) {
                ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n\"" + selectedsvtplaydl + "\" " + tr("is decompressed."));
                ui->progressBar->setValue(100);

                // Remove compressed file

                if(QFile::remove(*svtplay_dlPath + "/" + selectedsvtplaydl)) {
                    ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n" + tr("Removed") + " \"" + selectedsvtplaydl + "\".");
                    //                    QTimer::singleShot(5000, this, &DownloadUnpack::setAllEnabled);
                    ui->actionDownload->setEnabled(true);
                    ui->actionDownloadLatest->setEnabled(true);
                    ui->actionDownloadLatestStable->setEnabled(true);
                    ui->actionDownloadToBleedingedge->setEnabled(true);
                    ui->actionDownloadToStable->setEnabled(true);
                    ui->actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setEnabled(true);
                    ui->actionDownloadLatestStableToDirectoryStable->setEnabled(true);
                    ui->actionSearch->setEnabled(true);
                    ui->actionSearchLatestStable->setEnabled(true);
                    ui->actionExit->setEnabled(true);
                } else {
                    ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n" + tr("Could not delete") + " \"" + selectedsvtplaydl + "\".");
                    //                    QTimer::singleShot(5000, this, &DownloadUnpack::setAllEnabled);
                    ui->actionDownload->setEnabled(true);
                    ui->actionDownloadLatest->setEnabled(true);
                    ui->actionDownloadLatestStable->setEnabled(true);
                    ui->actionDownloadToBleedingedge->setEnabled(true);
                    ui->actionDownloadToStable->setEnabled(true);
                    ui->actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setEnabled(true);
                    ui->actionDownloadLatestStableToDirectoryStable->setEnabled(true);
                    ui->actionSearch->setEnabled(true);
                    ui->actionSearchLatestStable->setEnabled(true);
                    ui->actionExit->setEnabled(true);
                }

                // STABLE OR BLEEDINGRDGE

                if(stable_or_snapshot == 1 || stable_or_snapshot == 2) {
                    // ipac RENAME
                    QString path = *svtplay_dlPath;
                    QString nydir;
                    QString svtplaydlFolder;

                    if(QSysInfo::productType() == "windows") {
                        svtplaydlFolder = selectedsvtplaydl.mid(0, selectedsvtplaydl.size() - 4);

                        if(selectedsvtplaydl.right(7) == ".tar.gz") {
                            svtplaydlFolder = selectedsvtplaydl.mid(0, selectedsvtplaydl.size() - 7);
                            ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n\n" + tr("Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system."));
                            return;
                        }
                    } else {
                        svtplaydlFolder = selectedsvtplaydl.mid(0, selectedsvtplaydl.size() - 7);

                        if(selectedsvtplaydl.right(4) == ".zip") {
                            svtplaydlFolder = selectedsvtplaydl.mid(0, selectedsvtplaydl.size() - 4);
                            ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n\n" + tr("Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system."));
                            return;
                        }
                    }

                    QString olddir = path + "/" + svtplaydlFolder;

                    if(stable_or_snapshot == 2) {
                        nydir = path + "/beta";
                        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                                           LIBRARY_NAME);
                        settings.beginGroup("Path");
                        settings.setValue("betapath", path);
                        settings.endGroup();
                    } else if(stable_or_snapshot == 1) {
                        nydir = path + "/stable";
                        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                                           LIBRARY_NAME);
                        settings.beginGroup("Path");
                        settings.setValue("stablepath", path);
                        settings.endGroup();
                    } else {
                        nydir = path;
                    }

                    //                process->waitForFinished();
                    delete process;
                    // COPY
                    // Delete old dir
                    QDir dolddir(olddir);
                    //                QDir dnydir(nydir);

                    if(!dolddir.exists()) {
                        ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n\n" + tr("Download failed, please try again."));
                    }

                    Rename *myRename = new Rename;

                    if(myRename->reName(olddir, nydir)) {
                        //                        QString namn;
                        if(stable_or_snapshot == 1) {
                            ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n" + tr("Changed name from") + " \"" + svtplaydlFolder  + "\" " + tr("to") + " \"stable\".");
                        } else if(stable_or_snapshot == 2) {
                            ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n" + tr("Changed name from") + " \"" + svtplaydlFolder  + "\" " + tr("to") + " \"beta\".");
                        }
                    }

                    if(stable_or_snapshot == 2) {
                        ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n\n" + tr("Select \"svtplay-dl\", \"Use svtplay-dl beta\" to use this version."));
                    } else if(stable_or_snapshot == 1) {
                        ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n\n" + tr("Select \"svtplay-dl\", \"Use svtplay-dl stable\" to use this version."));
                    }
                } // end STABLE OR BLEEDINGRDGE
            }
        });

#ifdef Q_OS_WINDOWS
        //          QString cmd("powershell");
        QString EXECUTABLE = QCoreApplication::applicationDirPath() + "/7za.exe";
        QStringList parameters;
        parameters << "x" << "-y" << *svtplay_dlPath + "/" + selectedsvtplaydl;
        process->start(EXECUTABLE, parameters);
#endif
#ifdef Q_OS_LINUX
        QDir dir(*svtplay_dlPath);

        if(dir.mkpath(*svtplay_dlPath)) {
            process->setWorkingDirectory(*svtplay_dlPath);
            QStringList parameters;
            QString cmd;

            if(selectedsvtplaydl.right(4) ==  ".zip") {
                cmd = "unzip";
                parameters  << "-o" << selectedsvtplaydl;
            } else {
                cmd = "tar";
                parameters << "-xf" << selectedsvtplaydl << "-C" << *svtplay_dlPath;
            }

            process->start(cmd, parameters);
        } else {
            ui->pteEdit->setPlainText(ui->pteEdit->toPlainText()  + "\n\"" + tr("Failed to decompress.") + "\" " + selectedsvtplaydl + "\"");
        }

#endif \
    // END TRY TO UNZIP
        reply->deleteLater();
    });
}


void DownloadUnpack::setAllEnabled()
{
    ui->actionDownload->setEnabled(true);
    ui->actionDownloadLatest->setEnabled(true);
    ui->actionDownloadLatestStable->setEnabled(true);
    ui->actionDownloadToBleedingedge->setEnabled(true);
    ui->actionDownloadToStable->setEnabled(true);
    ui->actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setEnabled(true);
    ui->actionDownloadLatestStableToDirectoryStable->setEnabled(true);
    ui->actionSearch->setEnabled(true);
    ui->actionSearchLatestStable->setEnabled(true);
    ui->actionExit->setEnabled(true);
    return;
}
