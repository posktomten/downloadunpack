// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadunpack
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/downloadunpack
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QFileInfo>
#include <QSettings>
#include <QStandardPaths>

#include "downloadunpack.h"


QString DownloadUnpack::getStablebetapath(QString stable_bleeding)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, EXECUTABLE_NAME);
    settings.beginGroup("Settings");
    bool portable = settings.value("portable", false).toBool();
    settings.endGroup();
    QString path;

    if(portable) {
        path = QApplication::applicationDirPath();
    } else {
        path = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);
    }

    QSettings settings2(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
    settings2.beginGroup("Path");
    QString svtplay_dlpath = settings2.value(stable_bleeding, path).toString();
    settings2.endGroup();
    QFileInfo fi(svtplay_dlpath);

    if(!fi.exists()) {
        svtplay_dlpath = path;
    }

    return svtplay_dlpath;
}
