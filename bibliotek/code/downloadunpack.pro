#// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          downloadunpack
#//          Copyright (C) 2020 - 2023 Ingemar Ceicer
#//          https://gitlab.com/posktomten/downloadunpack
#//          programming@ceicer.com
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License as published by
#//   the Free Software Foundation, either version 3 of the License, or
#//   (at your option) any later version.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


# 2021-05-18
# Updated 2021-12-18 15:20
# Default to uncompress and delete compressed file
QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
#win32:CONFIG+=staticlib
CONFIG+=staticlib

HEADERS += \
    downloadunpack.h \
    downloadunpack_global.h \
    rename.h \
    rename_global.h \
    ui_downloadunpack.h
 


SOURCES += \
    download.cpp \
    download_latest.cpp \
    downloadunpack.cpp \
    filedialog.cpp \
    get_stablepath_or_bleedingedgepath.cpp \
    networkerrormessages.cpp \
    rename.cpp \
    screenshot.cpp \
    setget_config.cpp \
    zoom.cpp 


	

FORMS += \
    downloadunpack.ui 


# UI_DIR = ../code


TRANSLATIONS += i18n/_libdownloadunpack_it_IT.ts \
                i18n/_libdownloadunpack_sv_SE.ts \
                i18n/_libdownloadunpack_template_xx_XX.ts

 RESOURCES += \
 resource_downloadunpack.qrc


#Export LIBS
DEFINES += DOWNLOADUNPACK_LIBRARY
DEFINES += RENAME_LIBRARY

TEMPLATE = lib

# By default, qmake will make a shared library. Uncomment to make the library
#CONFIG += staticlib

CONFIG(release, debug|release):BUILD=RELEASE
CONFIG(debug, debug|release):BUILD=DEBUG

equals(BUILD,RELEASE) {

    TARGET=downloadunpack

}
equals(BUILD,DEBUG) {

    TARGET=downloadunpackd
}

#messalib6ge( $$OUT_PWD )

equals(QT_MAJOR_VERSION, 5) {
DESTDIR=$$OUT_PWD-lib
# DESTDIR=C:\Users\posktomten\PROGRAMMERING\streamcapture2\lib5
# DESTDIR=/home/ingemar/PROGRAMMERING/streamcapture2/lib5
}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR=$$OUT_PWD-lib
#DESTDIR=C:\Users\posktomten\PROGRAMMERING\streamcapture2\lib6_static
# DESTDIR=/home/ingemar/PROGRAMMERING/streamcapture2/lib6
}
message (--------------------------------------------------)
message (OS: $$QMAKE_HOST.os)
message (Arch: $$QMAKE_HOST.arch)
message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (qmake path: $$QMAKE_QMAKE)
message (Qt version: $$QT_VERSION)
message(*.pro path: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(LIBS: $$LIBS)
message(PWD: $$PWD)
message (--------------------------------------------------)


