<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE" sourcelanguage="en_US">
<context>
    <name>DownloadUnpack</name>
    <message>
        <location filename="../download.cpp" line="68"/>
        <source>Is not writable. Cancels</source>
        <translation>Är inte skrivbar. Avbryter</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="72"/>
        <source>Starting to download...
</source>
        <translation>Börjar att ladda ner...
</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="98"/>
        <location filename="../download.cpp" line="227"/>
        <location filename="../download.cpp" line="229"/>
        <source>to</source>
        <translation>till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="98"/>
        <location filename="../download.cpp" line="100"/>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Kontrollera dina filbehörigheter och antivirusprogram.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="98"/>
        <source>Can not save</source>
        <translation>Kan inte spara</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="100"/>
        <source>Can not save to</source>
        <translation>Kan inte spara till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="102"/>
        <source>is downloaded to</source>
        <translation>är nedladdad till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="115"/>
        <source>Starting to decompress</source>
        <translation>Börjar dekomprimera</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="132"/>
        <source>is decompressed.</source>
        <translation>är dekomprimerad.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="138"/>
        <source>Removed</source>
        <translation>Tog bort</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="151"/>
        <source>Could not delete</source>
        <translation>Kunde inte ta bort</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="178"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation>Varning! Den version av svtplay-dl som du laddade ner är avsedd för Linux operativsystem. Det kommer inte att fungera med Windows operativsystem.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="186"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation>Varning! Den version av svtplay-dl som du laddade ner är avsedd för Windows operativsystem. Det kommer inte att fungera med Linux operativsystem.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="219"/>
        <source>Download failed, please try again.</source>
        <translation>Nedladdningen misslyckades, försök igen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="227"/>
        <location filename="../download.cpp" line="229"/>
        <source>Changed name from</source>
        <translation>Ändrade namn från</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="234"/>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation>Välj &quot;svtplay-dl&quot;, &quot;Använd svtpay-dl beta&quot; för att använda den här versionen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="236"/>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation>Välj &quot;svtplay-dl&quot;, &quot;Använd svtpay-dl stabila&quot; för att använda den här versionen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="267"/>
        <source>Failed to decompress.</source>
        <translation>Misslyckades med att dekomprimera.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="233"/>
        <source>Current location for stable:</source>
        <translation>Nuvarande plats för stabila:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="243"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Ta en skärmdump (5 sekunders fördröjning)</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="257"/>
        <location filename="../downloadunpack.cpp" line="261"/>
        <source>For</source>
        <translation>För</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="296"/>
        <location filename="../downloadunpack.cpp" line="331"/>
        <location filename="../downloadunpack.cpp" line="404"/>
        <location filename="../downloadunpack.cpp" line="430"/>
        <source>will be downloaded to</source>
        <translation>Kommer att laddas ner till</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="365"/>
        <source>svtplay-dl stable version</source>
        <translation>svtplay-dl stabila versionen</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;svtplay-dl&quot;, &quot;Use stable svtplay-dl&quot;.</source>
        <translation type="vanished">För att använda denn version, Klicka
&quot;svtplay-dl&quot;, &quot;Använd svtplay-dl stable&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="331"/>
        <location filename="../downloadunpack.cpp" line="430"/>
        <source>To use this svtplay-dl version, click
&quot;svtplay-dl&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation>För att använda denn version, Klicka
&quot;svtplay-dl&quot;, &quot;Använd svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="345"/>
        <location filename="../downloadunpack.cpp" line="404"/>
        <location filename="../downloadunpack.cpp" line="430"/>
        <source>Selected svtplay-dl</source>
        <translation>Vald svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="296"/>
        <location filename="../downloadunpack.cpp" line="404"/>
        <source>To use this svtplay-dl version, click
&quot;svtplay-dl&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation>För att använda denn version, Klicka
&quot;svtplay-dl&quot;, &quot;Använd stabila svtplay-dl&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="131"/>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation>Ladda ner svtplay-dl från bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="345"/>
        <location filename="../downloadunpack.cpp" line="365"/>
        <location filename="../downloadunpack.cpp" line="385"/>
        <source>will be downloaded.</source>
        <translation>kommer att laddas ner.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="233"/>
        <source>Current location for beta:</source>
        <translation>Nuvarande plats för beta:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="296"/>
        <source>The stable version of svtplay-dl</source>
        <translation>Den stabila versionen av svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="331"/>
        <source>The beta version of svtplay-dl</source>
        <translation>Betaversionen av svtplay-dl</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation type="vanished">För att använda denn version, Klicka
&quot;Verktyg&quot;, &quot;Använd svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="570"/>
        <source>files</source>
        <translation>filer</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="572"/>
        <source>file</source>
        <translation>fil</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="37"/>
        <location filename="../ui_downloadunpack.h" line="338"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="202"/>
        <location filename="../ui_downloadunpack.h" line="429"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="237"/>
        <location filename="../ui_downloadunpack.h" line="432"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="294"/>
        <location filename="../ui_downloadunpack.h" line="348"/>
        <source>Try to decompress</source>
        <translation>Försöker att dekomprimera</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="343"/>
        <location filename="../ui_downloadunpack.h" line="357"/>
        <source>Source code...</source>
        <translation>Källkod...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="351"/>
        <location filename="../ui_downloadunpack.h" line="358"/>
        <source>Binary files...</source>
        <translation>Binära filer...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="303"/>
        <location filename="../ui_downloadunpack.h" line="349"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="241"/>
        <location filename="../ui_downloadunpack.h" line="433"/>
        <source>Choose location for</source>
        <translation>Välj platsen för</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="219"/>
        <location filename="../ui_downloadunpack.h" line="430"/>
        <source>V&amp;isit</source>
        <translation>&amp;Besök</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="226"/>
        <location filename="../ui_downloadunpack.h" line="431"/>
        <source>&amp;View</source>
        <translation>&amp;Visa</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="262"/>
        <location filename="../ui_downloadunpack.h" line="339"/>
        <source>Search beta</source>
        <translation>Sök beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="265"/>
        <location filename="../downloadunpack.ui" line="428"/>
        <location filename="../downloadunpack.ui" line="456"/>
        <location filename="../downloadunpack.ui" line="510"/>
        <location filename="../ui_downloadunpack.h" line="341"/>
        <location filename="../ui_downloadunpack.h" line="384"/>
        <location filename="../ui_downloadunpack.h" line="390"/>
        <location filename="../ui_downloadunpack.h" line="405"/>
        <source>&quot;&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="277"/>
        <location filename="../downloadunpack.ui" line="280"/>
        <location filename="../ui_downloadunpack.h" line="343"/>
        <location filename="../ui_downloadunpack.h" line="344"/>
        <source>Download...</source>
        <translation>Ladda ner...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="283"/>
        <location filename="../ui_downloadunpack.h" line="346"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="306"/>
        <location filename="../ui_downloadunpack.h" line="351"/>
        <source>F4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="315"/>
        <location filename="../ui_downloadunpack.h" line="353"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="324"/>
        <location filename="../ui_downloadunpack.h" line="354"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="329"/>
        <location filename="../ui_downloadunpack.h" line="355"/>
        <source>Load external language file...</source>
        <translation>Ladda extern språkfil...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="338"/>
        <location filename="../ui_downloadunpack.h" line="356"/>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="381"/>
        <location filename="../ui_downloadunpack.h" line="366"/>
        <source>Ctrl++</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="389"/>
        <location filename="../ui_downloadunpack.h" line="370"/>
        <source>Ctrl+0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="401"/>
        <location filename="../ui_downloadunpack.h" line="374"/>
        <source>Ctrl+-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="406"/>
        <location filename="../downloadunpack.ui" line="409"/>
        <location filename="../ui_downloadunpack.h" line="376"/>
        <location filename="../ui_downloadunpack.h" line="378"/>
        <source>Show all Toolbars</source>
        <translation>Visa alla verktygsfält</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="489"/>
        <location filename="../downloadunpack.ui" line="492"/>
        <location filename="../downloadunpack.ui" line="495"/>
        <location filename="../ui_downloadunpack.h" line="397"/>
        <location filename="../ui_downloadunpack.h" line="398"/>
        <location filename="../ui_downloadunpack.h" line="400"/>
        <source>beta...</source>
        <translation>beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="504"/>
        <location filename="../downloadunpack.ui" line="507"/>
        <location filename="../ui_downloadunpack.h" line="402"/>
        <location filename="../ui_downloadunpack.h" line="403"/>
        <source>Search stable</source>
        <translation>Sök stabila</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="519"/>
        <location filename="../ui_downloadunpack.h" line="407"/>
        <source>Download stable...</source>
        <translation>Ladda ner stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="522"/>
        <location filename="../ui_downloadunpack.h" line="408"/>
        <source>Download
stable...</source>
        <translation>Ladda ner
stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="526"/>
        <location filename="../ui_downloadunpack.h" line="411"/>
        <source>Download stable</source>
        <translation>Ladda ner stabila</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="535"/>
        <location filename="../ui_downloadunpack.h" line="413"/>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="538"/>
        <location filename="../ui_downloadunpack.h" line="414"/>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila till
mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="542"/>
        <location filename="../ui_downloadunpack.h" line="417"/>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila
till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="552"/>
        <location filename="../ui_downloadunpack.h" line="420"/>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation>Ladda ner beta till mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="555"/>
        <location filename="../downloadunpack.ui" line="559"/>
        <location filename="../ui_downloadunpack.h" line="421"/>
        <location filename="../ui_downloadunpack.h" line="424"/>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation>Ladda ner beta till
mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="565"/>
        <location filename="../ui_downloadunpack.h" line="427"/>
        <source>Show Default Toolbar</source>
        <translation>Visa standardverktygsfältet</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="369"/>
        <location filename="../ui_downloadunpack.h" line="362"/>
        <source>Download latest</source>
        <translation>Ladda ner senaste</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="474"/>
        <location filename="../downloadunpack.ui" line="477"/>
        <location filename="../downloadunpack.ui" line="480"/>
        <location filename="../ui_downloadunpack.h" line="392"/>
        <location filename="../ui_downloadunpack.h" line="393"/>
        <location filename="../ui_downloadunpack.h" line="395"/>
        <source>stable...</source>
        <translation>stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="449"/>
        <location filename="../ui_downloadunpack.h" line="386"/>
        <source>Download to directory &quot;stable&quot;</source>
        <translation>Ladda ner till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="363"/>
        <location filename="../downloadunpack.ui" line="366"/>
        <location filename="../ui_downloadunpack.h" line="359"/>
        <location filename="../ui_downloadunpack.h" line="360"/>
        <source>Download beta...</source>
        <translation>Ladda ner beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="421"/>
        <location filename="../ui_downloadunpack.h" line="380"/>
        <source>Download to directory &quot;beta&quot;</source>
        <translation>Ladda ner till mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="424"/>
        <location filename="../ui_downloadunpack.h" line="381"/>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation>Ladda ner till
mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="452"/>
        <location filename="../ui_downloadunpack.h" line="387"/>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation>Ladda ner till
mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="378"/>
        <location filename="../ui_downloadunpack.h" line="364"/>
        <source>Zoom In</source>
        <translation>Zooma in</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="386"/>
        <location filename="../ui_downloadunpack.h" line="368"/>
        <source>Zoom Default</source>
        <translation>Zooma som standard</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="398"/>
        <location filename="../ui_downloadunpack.h" line="372"/>
        <source>Zoom Out</source>
        <translation>Zooma ut</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="464"/>
        <location filename="../downloadunpack.cpp" line="489"/>
        <location filename="../filedialog.cpp" line="42"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="463"/>
        <location filename="../downloadunpack.cpp" line="488"/>
        <location filename="../filedialog.cpp" line="41"/>
        <source>Choose</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="158"/>
        <source>Select svtplay-dl and download to the &quot;stable&quot; folder and to the &quot;beta&quot; folder.</source>
        <translation>Välj svtplay-dl och ladda ner till  mappen &quot;stable&quot; och till mappen &quot;beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="182"/>
        <source>Direct download of stable to the &quot;stable&quot; folder and beta to the &quot;beta&quot; folder.</source>
        <translation>Direkt nedladdning av stable till &quot;stable&quot;-mappen och beta till &quot;beta&quot;-mappen.</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use stable svtplay-dl&quot;.</source>
        <translation type="vanished">För att använda denna svtplay-dl, klicka
&quot;Verktyg&quot;, &quot;Använd stabila svtplay-dl&quot;.</translation>
    </message>
    <message>
        <source>stable svtplay-dl</source>
        <translation type="vanished">stabila svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="385"/>
        <source>svtplay-dl beta</source>
        <translation>svtplay-dl beta</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation type="vanished">För att använda denna version, klicka
&quot;Verktyg&quot;, Använd stabila svtplay-dl&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="465"/>
        <location filename="../downloadunpack.cpp" line="490"/>
        <source>Save svtplay-dl in directory</source>
        <translation>Spara svtplay-dl i mappen</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="38"/>
        <source>The font size changes to the selected font size</source>
        <translation>Teckenstorleken ändras till vald teckenstorlek</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="44"/>
        <source>Save a screenshot</source>
        <translation>Spara en skärmdump</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="46"/>
        <source>Images (*.png)</source>
        <translation>bildfiler (*.png)</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="31"/>
        <source>No error.</source>
        <translation>Inga fel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="35"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Fjärrservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="39"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Fjärrvärdnamnet hittades inte (ogiltigt värdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="43"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Anslutningen till fjärrservern tog för lång tid.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="47"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>Operationen avbröts via anrop till abort() eller close() innan den avslutades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="51"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>SSL/TLS-handskakningen misslyckades och den krypterade kanalen kunde inte upprättas. Signalen sslErrors() borde ha sänts ut.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="55"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket, men systemet har initierat roaming till en annan åtkomstpunkt. Begäran bör skickas in på nytt och kommer att behandlas så snart anslutningen återupprättats.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="59"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket eller misslyckande med att starta nätverket.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="63"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>Bakgrundsbegäran är för närvarande inte tillåten på grund av plattformspolicy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="67"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>Omdirigeringarna nådde maxgränsen. Gränsen är som standard inställd på 50 eller enligt QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="71"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Vid omdirigeringar upptäckte API:et för nätverksåtkomst en omdirigering från ett krypterat protokoll (https) till ett okrypterat (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="75"/>
        <source>An unknown network-related error was detected.</source>
        <translation>Ett okänt nätverksrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="79"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>Anslutningen till proxyservern nekades (proxyservern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="83"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Proxyservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="87"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Proxyvärdnamnet hittades inte (ogiltigt proxyvärdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="91"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>Anslutningen till proxyn tog för läng tid eller så svarade proxyn inte i tid på den skickade begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="95"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Proxyn kräver autentisering för att uppfylla begäran men accepterade inte erbjudna användarnamn och lösenord (eller så har inga användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="99"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>Ett okänt proxyrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="103"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>Åtkomsten till fjärrinnehållet nekades (liknande HTTP-fel 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="107"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>Åtgärden som begärs på fjärrinnehållet är inte tillåten.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="111"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Fjärrinnehållet hittades inte på servern (liknande HTTP-fel 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="115"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Fjärrservern kräver autentisering för att visa innehållet men angivna användarnamn och lösenord accepterades inte (eller så har inget användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="119"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>Begäran behövde skickas igen, men detta misslyckades till exempel eftersom uppladdningsdata inte kunde läsas en andra gång.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="123"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Begäran kunde inte slutföras på grund av en konflikt med resursens nuvarande tillstånd.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="127"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>Den begärda resursen är inte längre tillgänglig på servern.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="131"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>Ett okänt fel relaterat till fjärrinnehållet upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="135"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>Network Access API kan inte uppfylla begäran eftersom protokollet inte är känt.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="139"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>Den begärda åtgärden är ogiltig för detta protokoll.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="143"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>Ett haveri i protokollet upptäcktes (analysfel, ogiltiga eller oväntade svar, etc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="147"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Servern stötte på ett oväntat tillstånd som hindrade den från att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="151"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Servern stöder inte den funktionalitet som krävs för att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="155"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>Servern kan inte hantera begäran just nu.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="159"/>
        <source>Unknown Server Error.</source>
        <translation>Okänt serverfel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="163"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Fjärrservern vägrade anslutningen (servern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="167"/>
        <source>Unknown Error.</source>
        <translation>Okänt fel.</translation>
    </message>
    <message>
        <location filename="../filedialog.cpp" line="43"/>
        <source>Save </source>
        <translation>Spara </translation>
    </message>
    <message>
        <location filename="../filedialog.cpp" line="43"/>
        <source> in directory</source>
        <translation> i mappen</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="136"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
</context>
</TS>
