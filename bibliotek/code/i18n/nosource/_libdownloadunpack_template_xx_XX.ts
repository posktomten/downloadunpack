<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>DownloadUnpack</name>
    <message>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose location for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Try to decompress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Binary files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download beta...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show all Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download to directory &quot;beta&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beta...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search stable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download stable...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download
stable...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download stable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Default Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download to directory &quot;stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download latest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>stable...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>V&amp;isit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load external language file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Is not writable. Cancels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starting to download...
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can not save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check your file permissions and antivirus software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can not save to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>is downloaded to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starting to decompress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>is decompressed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download failed, please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Changed name from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed to decompress.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>For</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>will be downloaded to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The beta version of svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current location for stable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Selected svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current location for beta:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select svtplay-dl and download to the &quot;stable&quot; folder and to the &quot;beta&quot; folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Direct download of stable to the &quot;stable&quot; folder and beta to the &quot;beta&quot; folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The stable version of svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use stable svtplay-dl&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>will be downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>stable svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <oldsource>Cancell</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save svtplay-dl in directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The font size changes to the selected font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save a screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Images (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the remote server timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown network-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown proxy-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The requested resource is no longer available at the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown error related to the remote content was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The requested operation is invalid for this protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server is unable to handle the request at this time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown Server Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> in directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
