<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE" sourcelanguage="en_US">
<context>
    <name>DownloadUnpack</name>
    <message>
        <source>Is not writable. Cancels</source>
        <translation>Är inte skrivbar. Avbryter</translation>
    </message>
    <message>
        <source>Starting to download...
</source>
        <translation>Börjar att ladda ner...
</translation>
    </message>
    <message>
        <source>to</source>
        <translation>till</translation>
    </message>
    <message>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Kontrollera dina filbehörigheter och antivirusprogram.</translation>
    </message>
    <message>
        <source>Can not save</source>
        <translation>Kan inte spara</translation>
    </message>
    <message>
        <source>Can not save to</source>
        <translation>Kan inte spara till</translation>
    </message>
    <message>
        <source>is downloaded to</source>
        <translation>är nedladdad till</translation>
    </message>
    <message>
        <source>Starting to decompress</source>
        <translation>Börjar dekomprimera</translation>
    </message>
    <message>
        <source>is decompressed.</source>
        <translation>är dekomprimerad.</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation>Tog bort</translation>
    </message>
    <message>
        <source>Could not delete</source>
        <translation>Kunde inte ta bort</translation>
    </message>
    <message>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation>Varning! Den version av svtplay-dl som du laddade ner är avsedd för Linux operativsystem. Det kommer inte att fungera med Windows operativsystem.</translation>
    </message>
    <message>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation>Varning! Den version av svtplay-dl som du laddade ner är avsedd för Windows operativsystem. Det kommer inte att fungera med Linux operativsystem.</translation>
    </message>
    <message>
        <source>Download failed, please try again.</source>
        <translation>Nedladdningen misslyckades, försök igen.</translation>
    </message>
    <message>
        <source>Changed name from</source>
        <translation>Ändrade namn från</translation>
    </message>
    <message>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation>Välj &quot;svtplay-dl&quot;, &quot;Använd svtpay-dl beta&quot; för att använda den här versionen.</translation>
    </message>
    <message>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation>Välj &quot;svtplay-dl&quot;, &quot;Använd svtpay-dl stabila&quot; för att använda den här versionen.</translation>
    </message>
    <message>
        <source>Failed to decompress.</source>
        <translation>Misslyckades med att dekomprimera.</translation>
    </message>
    <message>
        <source>Current location for stable:</source>
        <translation>Nuvarande plats för stabila:</translation>
    </message>
    <message>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Ta en skärmdump (5 sekunders fördröjning)</translation>
    </message>
    <message>
        <source>For</source>
        <translation>För</translation>
    </message>
    <message>
        <source>will be downloaded to</source>
        <translation>Kommer att laddas ner till</translation>
    </message>
    <message>
        <source>Selected svtplay-dl</source>
        <translation>Vald svtplay-dl</translation>
    </message>
    <message>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation>Ladda ner svtplay-dl från bin.ceicer.com</translation>
    </message>
    <message>
        <source>will be downloaded.</source>
        <translation>kommer att laddas ner.</translation>
    </message>
    <message>
        <source>Current location for beta:</source>
        <translation>Nuvarande plats för beta:</translation>
    </message>
    <message>
        <source>The stable version of svtplay-dl</source>
        <translation>Den stabila versionen av svtplay-dl</translation>
    </message>
    <message>
        <source>The beta version of svtplay-dl</source>
        <translation>Betaversionen av svtplay-dl</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation>För att använda denn version, Klicka
&quot;Verktyg&quot;, &quot;Använd svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <source>files</source>
        <translation>filer</translation>
    </message>
    <message>
        <source>file</source>
        <translation>fil</translation>
    </message>
    <message>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <source>Try to decompress</source>
        <translation>Försöker att dekomprimera</translation>
    </message>
    <message>
        <source>Source code...</source>
        <translation>Källkod...</translation>
    </message>
    <message>
        <source>Binary files...</source>
        <translation>Binära filer...</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <source>Choose location for</source>
        <translation>Välj platsen för</translation>
    </message>
    <message>
        <source>V&amp;isit</source>
        <translation>&amp;Besök</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Visa</translation>
    </message>
    <message>
        <source>Search beta</source>
        <translation>Sök beta</translation>
    </message>
    <message>
        <source>&quot;&quot;</source>
        <translation></translation>
    </message>
    <message>
        <source>Download...</source>
        <translation>Ladda ner...</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <source>F4</source>
        <translation></translation>
    </message>
    <message>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <source>Load external language file...</source>
        <translation>Ladda extern språkfil...</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <source>Ctrl++</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+0</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+-</source>
        <translation></translation>
    </message>
    <message>
        <source>Show all Toolbars</source>
        <translation>Visa alla verktygsfält</translation>
    </message>
    <message>
        <source>beta...</source>
        <translation>beta...</translation>
    </message>
    <message>
        <source>Search stable</source>
        <translation>Sök stabila</translation>
    </message>
    <message>
        <source>Download stable...</source>
        <translation>Ladda ner stabila...</translation>
    </message>
    <message>
        <source>Download
stable...</source>
        <translation>Ladda ner
stabila...</translation>
    </message>
    <message>
        <source>Download stable</source>
        <translation>Ladda ner stabila</translation>
    </message>
    <message>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila till
mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila
till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation>Ladda ner beta till mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation>Ladda ner beta till
mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <source>Show Default Toolbar</source>
        <translation>Visa standardverktygsfältet</translation>
    </message>
    <message>
        <source>Download latest</source>
        <translation>Ladda ner senaste</translation>
    </message>
    <message>
        <source>stable...</source>
        <translation>stabila...</translation>
    </message>
    <message>
        <source>Download to directory &quot;stable&quot;</source>
        <translation>Ladda ner till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <source>Download beta...</source>
        <translation>Ladda ner beta...</translation>
    </message>
    <message>
        <source>Download to directory &quot;beta&quot;</source>
        <translation>Ladda ner till mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation>Ladda ner till
mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation>Ladda ner till
mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation>Zooma in</translation>
    </message>
    <message>
        <source>Zoom Default</source>
        <translation>Zooma som standard</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation>Zooma ut</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <source>Choose</source>
        <translation>Välj</translation>
    </message>
    <message>
        <source>Select svtplay-dl and download to the &quot;stable&quot; folder and to the &quot;beta&quot; folder.</source>
        <translation>Välj svtplay-dl och ladda ner till  mappen &quot;stable&quot; och till mappen &quot;beta&quot;.</translation>
    </message>
    <message>
        <source>Direct download of stable to the &quot;stable&quot; folder and beta to the &quot;beta&quot; folder.</source>
        <translation>Direkt nedladdning av stable till &quot;stable&quot;-mappen och beta till &quot;beta&quot;-mappen.</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use stable svtplay-dl&quot;.</source>
        <translation>För att använda denna svtplay-dl, klicka
&quot;Verktyg&quot;, &quot;Använd stabila svtplay-dl&quot;.</translation>
    </message>
    <message>
        <source>stable svtplay-dl</source>
        <translation>stabila svtplay-dl</translation>
    </message>
    <message>
        <source>svtplay-dl beta</source>
        <translation>svtplay-dl beta</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation>För att använda denna version, klicka
&quot;Verktyg&quot;, Använd stabila svtplay-dl&quot;.</translation>
    </message>
    <message>
        <source>Save svtplay-dl in directory</source>
        <translation>Spara svtplay-dl i mappen</translation>
    </message>
    <message>
        <source>The font size changes to the selected font size</source>
        <translation>Teckenstorleken ändras till vald teckenstorlek</translation>
    </message>
    <message>
        <source>Save a screenshot</source>
        <translation>Spara en skärmdump</translation>
    </message>
    <message>
        <source>Images (*.png)</source>
        <translation>bildfiler (*.png)</translation>
    </message>
    <message>
        <source>No error.</source>
        <translation>Inga fel.</translation>
    </message>
    <message>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Fjärrservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Fjärrvärdnamnet hittades inte (ogiltigt värdnamn).</translation>
    </message>
    <message>
        <source>The connection to the remote server timed out.</source>
        <translation>Anslutningen till fjärrservern tog för lång tid.</translation>
    </message>
    <message>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>Operationen avbröts via anrop till abort() eller close() innan den avslutades.</translation>
    </message>
    <message>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>SSL/TLS-handskakningen misslyckades och den krypterade kanalen kunde inte upprättas. Signalen sslErrors() borde ha sänts ut.</translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket, men systemet har initierat roaming till en annan åtkomstpunkt. Begäran bör skickas in på nytt och kommer att behandlas så snart anslutningen återupprättats.</translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket eller misslyckande med att starta nätverket.</translation>
    </message>
    <message>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>Bakgrundsbegäran är för närvarande inte tillåten på grund av plattformspolicy.</translation>
    </message>
    <message>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>Omdirigeringarna nådde maxgränsen. Gränsen är som standard inställd på 50 eller enligt QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Vid omdirigeringar upptäckte API:et för nätverksåtkomst en omdirigering från ett krypterat protokoll (https) till ett okrypterat (http).</translation>
    </message>
    <message>
        <source>An unknown network-related error was detected.</source>
        <translation>Ett okänt nätverksrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>Anslutningen till proxyservern nekades (proxyservern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Proxyservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Proxyvärdnamnet hittades inte (ogiltigt proxyvärdnamn).</translation>
    </message>
    <message>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>Anslutningen till proxyn tog för läng tid eller så svarade proxyn inte i tid på den skickade begäran.</translation>
    </message>
    <message>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Proxyn kräver autentisering för att uppfylla begäran men accepterade inte erbjudna användarnamn och lösenord (eller så har inga användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <source>An unknown proxy-related error was detected.</source>
        <translation>Ett okänt proxyrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>Åtkomsten till fjärrinnehållet nekades (liknande HTTP-fel 403).</translation>
    </message>
    <message>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>Åtgärden som begärs på fjärrinnehållet är inte tillåten.</translation>
    </message>
    <message>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Fjärrinnehållet hittades inte på servern (liknande HTTP-fel 404).</translation>
    </message>
    <message>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Fjärrservern kräver autentisering för att visa innehållet men angivna användarnamn och lösenord accepterades inte (eller så har inget användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>Begäran behövde skickas igen, men detta misslyckades till exempel eftersom uppladdningsdata inte kunde läsas en andra gång.</translation>
    </message>
    <message>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Begäran kunde inte slutföras på grund av en konflikt med resursens nuvarande tillstånd.</translation>
    </message>
    <message>
        <source>The requested resource is no longer available at the server.</source>
        <translation>Den begärda resursen är inte längre tillgänglig på servern.</translation>
    </message>
    <message>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>Ett okänt fel relaterat till fjärrinnehållet upptäcktes.</translation>
    </message>
    <message>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>Network Access API kan inte uppfylla begäran eftersom protokollet inte är känt.</translation>
    </message>
    <message>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>Den begärda åtgärden är ogiltig för detta protokoll.</translation>
    </message>
    <message>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>Ett haveri i protokollet upptäcktes (analysfel, ogiltiga eller oväntade svar, etc.).</translation>
    </message>
    <message>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Servern stötte på ett oväntat tillstånd som hindrade den från att uppfylla begäran.</translation>
    </message>
    <message>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Servern stöder inte den funktionalitet som krävs för att uppfylla begäran.</translation>
    </message>
    <message>
        <source>The server is unable to handle the request at this time.</source>
        <translation>Servern kan inte hantera begäran just nu.</translation>
    </message>
    <message>
        <source>Unknown Server Error.</source>
        <translation>Okänt serverfel.</translation>
    </message>
    <message>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Fjärrservern vägrade anslutningen (servern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <source>Unknown Error.</source>
        <translation>Okänt fel.</translation>
    </message>
    <message>
        <source>Save </source>
        <translation>Spara </translation>
    </message>
    <message>
        <source> in directory</source>
        <translation> i mappen</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
</context>
</TS>
