<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>DownloadUnpack</name>
    <message>
        <source>MainWindow</source>
        <translation>Finestra principale</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation>&amp;Strumenti</translation>
    </message>
    <message>
        <source>Try to decompress</source>
        <translation>Prova a decomprimere</translation>
    </message>
    <message>
        <source>Source code...</source>
        <translation>Codice sorgente...</translation>
    </message>
    <message>
        <source>Binary files...</source>
        <translation>File binari...</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <source>Choose location for</source>
        <translation>Scegli percorso per</translation>
    </message>
    <message>
        <source>V&amp;isit</source>
        <translation>V&amp;isita</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <source>Search beta</source>
        <translation>Cerca beta</translation>
    </message>
    <message>
        <source>&quot;&quot;</source>
        <translation>&quot;&quot;</translation>
    </message>
    <message>
        <source>Download...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation>Svedese</translation>
    </message>
    <message>
        <source>Load external language file...</source>
        <translation>Carica file lingua esterno...</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <source>Ctrl++</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+0</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+-</source>
        <translation></translation>
    </message>
    <message>
        <source>Show all Toolbars</source>
        <translation>Visualizza tutte le barre strumenti</translation>
    </message>
    <message>
        <source>beta...</source>
        <translation>beta...</translation>
    </message>
    <message>
        <source>Search stable</source>
        <translation>Cerca stabile</translation>
    </message>
    <message>
        <source>Download stable...</source>
        <translation>Download stabile...</translation>
    </message>
    <message>
        <source>Download
stable...</source>
        <translation>Download stabile...</translation>
    </message>
    <message>
        <source>Download stable</source>
        <translation>Download stabile</translation>
    </message>
    <message>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation>Download stabile nella cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation>Download stabile nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation>Download stabile nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation>Download beta nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation>Download beta nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <source>Show Default Toolbar</source>
        <translation>Visualizza barra strumenti predefinita</translation>
    </message>
    <message>
        <source>Download latest</source>
        <translation>Download più recente</translation>
    </message>
    <message>
        <source>stable...</source>
        <translation>stabile...</translation>
    </message>
    <message>
        <source>Download to directory &quot;stable&quot;</source>
        <translation>Download nella cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <source>Download beta...</source>
        <translation>Download beta...</translation>
    </message>
    <message>
        <source>Download to directory &quot;beta&quot;</source>
        <translation>Download nella cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation>Download nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation>Download nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation>Zoom +</translation>
    </message>
    <message>
        <source>Zoom Default</source>
        <translation>Zoom predefinito</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation>Zoom -</translation>
    </message>
    <message>
        <source>Is not writable. Cancels</source>
        <translation>Non è scrivibile. Annulla</translation>
    </message>
    <message>
        <source>Starting to download...
</source>
        <translation>Avvio download...
</translation>
    </message>
    <message>
        <source>to</source>
        <translation>in</translation>
    </message>
    <message>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Verifica permessi file e software antivirus.</translation>
    </message>
    <message>
        <source>Can not save</source>
        <translation>Impossibile salvare</translation>
    </message>
    <message>
        <source>Can not save to</source>
        <translation>Impossibile salvare in</translation>
    </message>
    <message>
        <source>is downloaded to</source>
        <translation>è stato scaricato in</translation>
    </message>
    <message>
        <source>Starting to decompress</source>
        <translation>Avvio decompressione</translation>
    </message>
    <message>
        <source>is decompressed.</source>
        <translation>è stato decompresso.</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation>Rimosso</translation>
    </message>
    <message>
        <source>Could not delete</source>
        <translation>Impossibile eliminare</translation>
    </message>
    <message>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation>Attenzione! La versione di svtplay-dl che hai scaricato è destinata ai sistemi operativi Linux.
Non funzionerà con il sistema operativo Windows.</translation>
    </message>
    <message>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation>Attenzione! La versione di svtplay-dl che hai scaricato è destinata ai sistemi operativi Windows.
Non funzionerà con il sistema operativo Linux.</translation>
    </message>
    <message>
        <source>Download failed, please try again.</source>
        <translation>Download non riuscito, riprova.</translation>
    </message>
    <message>
        <source>Changed name from</source>
        <translation>Modifica nome da</translation>
    </message>
    <message>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation>Per usare questa versione seleziona &quot;svtplay-dl&quot; -&gt; &quot;Usa svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation>Per usare questa versione seleziona &quot;svtplay-dl&quot; -&gt; &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <source>Failed to decompress.</source>
        <translation>Decompressione fallita.</translation>
    </message>
    <message>
        <source>For</source>
        <translation>Per</translation>
    </message>
    <message>
        <source>will be downloaded to</source>
        <translation>verrà scaricato in</translation>
    </message>
    <message>
        <source>The beta version of svtplay-dl</source>
        <translation>La versione beta di svtplay-dl</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation>Per usare questa versione seleziona
&quot;Strumenti&quot; -&gt; &quot;Usa svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <source>files</source>
        <translation>file</translation>
    </message>
    <message>
        <source>file</source>
        <translation>file</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation>Download svtplay-dl da bin.ceicer.com</translation>
    </message>
    <message>
        <source>Current location for stable:</source>
        <translation>Percorso attuale versioen stabile:</translation>
    </message>
    <message>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Cattura una schermata (ritardo 5 secondi)</translation>
    </message>
    <message>
        <source>Selected svtplay-dl</source>
        <translation>svtplay-dl selezionato</translation>
    </message>
    <message>
        <source>Current location for beta:</source>
        <translation>Percorso attuale per beta:</translation>
    </message>
    <message>
        <source>Select svtplay-dl and download to the &quot;stable&quot; folder and to the &quot;beta&quot; folder.</source>
        <translation>Seleziona svtplay-dl e download nella cartella &quot;stable&quot; e nella cartella &quot;beta&quot;.</translation>
    </message>
    <message>
        <source>Direct download of stable to the &quot;stable&quot; folder and beta to the &quot;beta&quot; folder.</source>
        <translation>Download diretto della stabile nella cartella &quot;stable&quot; e beta nella cartella &quot;beta&quot;.</translation>
    </message>
    <message>
        <source>The stable version of svtplay-dl</source>
        <translation>La versione stabile di svtplay-dl</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use stable svtplay-dl&quot;.</source>
        <translation>Per usare questa versione di svtplay-dl, fai clic su
&quot;Strumenti&quot;, &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <source>will be downloaded.</source>
        <translation>verrà scaricato.</translation>
    </message>
    <message>
        <source>stable svtplay-dl</source>
        <translation>svtplay-dl stabile</translation>
    </message>
    <message>
        <source>svtplay-dl beta</source>
        <translation>svtplay-dl beta</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation>Per usare questa versione di svtplay-dl, fai clic su
&quot;Strumenti&quot;, &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <source>Choose</source>
        <translation>Scegli</translation>
    </message>
    <message>
        <source>Save svtplay-dl in directory</source>
        <translation>Salva svtplay-dl nella cartella</translation>
    </message>
    <message>
        <source>The font size changes to the selected font size</source>
        <translation>Dimensione tipo di carattere modificata come da dimensione selezionata</translation>
    </message>
    <message>
        <source>Save a screenshot</source>
        <translation>Salva schermata</translation>
    </message>
    <message>
        <source>Images (*.png)</source>
        <translation>Immagini (*.png)</translation>
    </message>
    <message>
        <source>No error.</source>
        <translation>Nessun errore.</translation>
    </message>
    <message>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server remoto ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Non è stato trovato il nome host remoto (nome host non valido).</translation>
    </message>
    <message>
        <source>The connection to the remote server timed out.</source>
        <translation>Timeout della connessione verso il server remoto.</translation>
    </message>
    <message>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>L&apos;operazione è stata annullata tramite chiamate a abort() o close() prima che fosse terminata.</translation>
    </message>
    <message>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>L&apos;handshake SSL/TLS non è riuscito e non è stato possibile stabilire il canale crittografato.&lt;br&gt;Dovrebbe essere stato emesso il segnale sslErrors().</translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete, tuttavia il sistema ha avviato il roaming verso un altro punto di accesso.&lt;br&gt;La richiesta deve essere ripresentata e sarà elaborata non appena la connessione sarà ristabilita.</translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete o del mancato avvio della rete.</translation>
    </message>
    <message>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>La richiesta in background non è attualmente consentita a causa della politica della piattaforma.</translation>
    </message>
    <message>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>È stato raggiunto il limite massimo durante i reindirizzamenti.&lt;br&gt;Per impostazione predefinita, il limite è impostato su 50 o impostato da QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Durante i reindirizzamenti, l&apos;API di accesso alla rete ha rilevato un reindirizzamento da un protocollo crittografato (https) a uno non crittografato (http).</translation>
    </message>
    <message>
        <source>An unknown network-related error was detected.</source>
        <translation>È stato rilevato un errore di rete sconosciuto.</translation>
    </message>
    <message>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>La connessione al server proxy è stata rifiutata (il server proxy non accetta richieste).</translation>
    </message>
    <message>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server proxy ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Non è stato trovato il nome host proxy (nome host proxy non valido).</translation>
    </message>
    <message>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>La connessione al proxy è scaduta oppure il proxy non ha risposto in tempo alla richiesta inviata.</translation>
    </message>
    <message>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Per elaborare la richiesta il proxy richiede l&apos;autenticazione ma non ha accettato (se presenti) le credenziali offerte.</translation>
    </message>
    <message>
        <source>An unknown proxy-related error was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al proxy.</translation>
    </message>
    <message>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>L&apos;accesso al contenuto remoto è stato negato (simile all&apos;errore HTTP 403).</translation>
    </message>
    <message>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>L&apos;operazione richiesta sul contenuto remoto non è consentita.</translation>
    </message>
    <message>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Nel server non è stato trovato il contenuto remoto (simile all&apos;errore HTTP 404).</translation>
    </message>
    <message>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Per elaborare la richiesta il server remoto richiede l&apos;autenticazione ma le credenziali fornite (se presenti) non sono state accettate.</translation>
    </message>
    <message>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>La richiesta doveva essere inviata di nuovo, ma questo non è riuscito ad esempio perché non è stato possibile leggere i dati di caricamento una seconda volta.</translation>
    </message>
    <message>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Impossibile completare la richiesta a causa di un conflitto con lo stato attuale della risorsa.</translation>
    </message>
    <message>
        <source>The requested resource is no longer available at the server.</source>
        <translation>La risorsa richiesta non è più disponibile nel server.</translation>
    </message>
    <message>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al contenuto remoto.</translation>
    </message>
    <message>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>L&apos;API di accesso alla rete non può soddisfare la richiesta perché il protocollo è sconosciuto.</translation>
    </message>
    <message>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>L&apos;operazione richiesta non è valida per questo protocollo.</translation>
    </message>
    <message>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>È stata rilevata un&apos;interruzione del protocollo (errore di analisi, risposte non valide o impreviste, ecc.).</translation>
    </message>
    <message>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Il server ha riscontrato una condizione imprevista che gli ha impedito di soddisfare la richiesta.</translation>
    </message>
    <message>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Il server non supporta la funzionalità necessaria per soddisfare la richiesta.</translation>
    </message>
    <message>
        <source>The server is unable to handle the request at this time.</source>
        <translation>In questo momento il server non è in grado di gestire la richiesta.</translation>
    </message>
    <message>
        <source>Unknown Server Error.</source>
        <translation>Errore server sconosciuto.</translation>
    </message>
    <message>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Il server remoto ha rifiutato la connessione (il server non accetta richieste).</translation>
    </message>
    <message>
        <source>Unknown Error.</source>
        <translation>Errore sconosciuto.</translation>
    </message>
    <message>
        <source>Save </source>
        <translation>Salva </translation>
    </message>
    <message>
        <source> in directory</source>
        <translation> nella cartella</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
</context>
</TS>
