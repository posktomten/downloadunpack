<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>DownloadUnpack</name>
    <message>
        <location filename="../downloadunpack.ui" line="37"/>
        <location filename="../ui_downloadunpack.h" line="338"/>
        <source>MainWindow</source>
        <translation>Finestra principale</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="202"/>
        <location filename="../ui_downloadunpack.h" line="429"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="237"/>
        <location filename="../ui_downloadunpack.h" line="432"/>
        <source>&amp;Tools</source>
        <translation>&amp;Strumenti</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="294"/>
        <location filename="../ui_downloadunpack.h" line="348"/>
        <source>Try to decompress</source>
        <translation>Prova a decomprimere</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="343"/>
        <location filename="../ui_downloadunpack.h" line="357"/>
        <source>Source code...</source>
        <translation>Codice sorgente...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="351"/>
        <location filename="../ui_downloadunpack.h" line="358"/>
        <source>Binary files...</source>
        <translation>File binari...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="303"/>
        <location filename="../ui_downloadunpack.h" line="349"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="241"/>
        <location filename="../ui_downloadunpack.h" line="433"/>
        <source>Choose location for</source>
        <translation>Scegli percorso per</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="219"/>
        <location filename="../ui_downloadunpack.h" line="430"/>
        <source>V&amp;isit</source>
        <translation>V&amp;isita</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="226"/>
        <location filename="../ui_downloadunpack.h" line="431"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="262"/>
        <location filename="../ui_downloadunpack.h" line="339"/>
        <source>Search beta</source>
        <translation>Cerca beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="265"/>
        <location filename="../downloadunpack.ui" line="428"/>
        <location filename="../downloadunpack.ui" line="456"/>
        <location filename="../downloadunpack.ui" line="510"/>
        <location filename="../ui_downloadunpack.h" line="341"/>
        <location filename="../ui_downloadunpack.h" line="384"/>
        <location filename="../ui_downloadunpack.h" line="390"/>
        <location filename="../ui_downloadunpack.h" line="405"/>
        <source>&quot;&quot;</source>
        <translation>&quot;&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="277"/>
        <location filename="../downloadunpack.ui" line="280"/>
        <location filename="../ui_downloadunpack.h" line="343"/>
        <location filename="../ui_downloadunpack.h" line="344"/>
        <source>Download...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="283"/>
        <location filename="../ui_downloadunpack.h" line="346"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="306"/>
        <location filename="../ui_downloadunpack.h" line="351"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="315"/>
        <location filename="../ui_downloadunpack.h" line="353"/>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="324"/>
        <location filename="../ui_downloadunpack.h" line="354"/>
        <source>Swedish</source>
        <translation>Svedese</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="329"/>
        <location filename="../ui_downloadunpack.h" line="355"/>
        <source>Load external language file...</source>
        <translation>Carica file lingua esterno...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="338"/>
        <location filename="../ui_downloadunpack.h" line="356"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="381"/>
        <location filename="../ui_downloadunpack.h" line="366"/>
        <source>Ctrl++</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="389"/>
        <location filename="../ui_downloadunpack.h" line="370"/>
        <source>Ctrl+0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="401"/>
        <location filename="../ui_downloadunpack.h" line="374"/>
        <source>Ctrl+-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="406"/>
        <location filename="../downloadunpack.ui" line="409"/>
        <location filename="../ui_downloadunpack.h" line="376"/>
        <location filename="../ui_downloadunpack.h" line="378"/>
        <source>Show all Toolbars</source>
        <translation>Visualizza tutte le barre strumenti</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="489"/>
        <location filename="../downloadunpack.ui" line="492"/>
        <location filename="../downloadunpack.ui" line="495"/>
        <location filename="../ui_downloadunpack.h" line="397"/>
        <location filename="../ui_downloadunpack.h" line="398"/>
        <location filename="../ui_downloadunpack.h" line="400"/>
        <source>beta...</source>
        <translation>beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="504"/>
        <location filename="../downloadunpack.ui" line="507"/>
        <location filename="../ui_downloadunpack.h" line="402"/>
        <location filename="../ui_downloadunpack.h" line="403"/>
        <source>Search stable</source>
        <translation>Cerca stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="519"/>
        <location filename="../ui_downloadunpack.h" line="407"/>
        <source>Download stable...</source>
        <translation>Download stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="522"/>
        <location filename="../ui_downloadunpack.h" line="408"/>
        <source>Download
stable...</source>
        <translation>Download stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="526"/>
        <location filename="../ui_downloadunpack.h" line="411"/>
        <source>Download stable</source>
        <translation>Download stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="535"/>
        <location filename="../ui_downloadunpack.h" line="413"/>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation>Download stabile nella cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="538"/>
        <location filename="../ui_downloadunpack.h" line="414"/>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation>Download stabile nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="542"/>
        <location filename="../ui_downloadunpack.h" line="417"/>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation>Download stabile nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="552"/>
        <location filename="../ui_downloadunpack.h" line="420"/>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation>Download beta nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="555"/>
        <location filename="../downloadunpack.ui" line="559"/>
        <location filename="../ui_downloadunpack.h" line="421"/>
        <location filename="../ui_downloadunpack.h" line="424"/>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation>Download beta nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="565"/>
        <location filename="../ui_downloadunpack.h" line="427"/>
        <source>Show Default Toolbar</source>
        <translation>Visualizza barra strumenti predefinita</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="369"/>
        <location filename="../ui_downloadunpack.h" line="362"/>
        <source>Download latest</source>
        <translation>Download più recente</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="474"/>
        <location filename="../downloadunpack.ui" line="477"/>
        <location filename="../downloadunpack.ui" line="480"/>
        <location filename="../ui_downloadunpack.h" line="392"/>
        <location filename="../ui_downloadunpack.h" line="393"/>
        <location filename="../ui_downloadunpack.h" line="395"/>
        <source>stable...</source>
        <translation>stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="449"/>
        <location filename="../ui_downloadunpack.h" line="386"/>
        <source>Download to directory &quot;stable&quot;</source>
        <translation>Download nella cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="363"/>
        <location filename="../downloadunpack.ui" line="366"/>
        <location filename="../ui_downloadunpack.h" line="359"/>
        <location filename="../ui_downloadunpack.h" line="360"/>
        <source>Download beta...</source>
        <translation>Download beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="421"/>
        <location filename="../ui_downloadunpack.h" line="380"/>
        <source>Download to directory &quot;beta&quot;</source>
        <translation>Download nella cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="424"/>
        <location filename="../ui_downloadunpack.h" line="381"/>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation>Download nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="452"/>
        <location filename="../ui_downloadunpack.h" line="387"/>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation>Download nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="378"/>
        <location filename="../ui_downloadunpack.h" line="364"/>
        <source>Zoom In</source>
        <translation>Zoom +</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="386"/>
        <location filename="../ui_downloadunpack.h" line="368"/>
        <source>Zoom Default</source>
        <translation>Zoom predefinito</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="398"/>
        <location filename="../ui_downloadunpack.h" line="372"/>
        <source>Zoom Out</source>
        <translation>Zoom -</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="68"/>
        <source>Is not writable. Cancels</source>
        <translation>Non è scrivibile. Annulla</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="72"/>
        <source>Starting to download...
</source>
        <translation>Avvio download...
</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="98"/>
        <location filename="../download.cpp" line="227"/>
        <location filename="../download.cpp" line="229"/>
        <source>to</source>
        <translation>in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="98"/>
        <location filename="../download.cpp" line="100"/>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Verifica permessi file e software antivirus.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="98"/>
        <source>Can not save</source>
        <translation>Impossibile salvare</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="100"/>
        <source>Can not save to</source>
        <translation>Impossibile salvare in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="102"/>
        <source>is downloaded to</source>
        <translation>è stato scaricato in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="115"/>
        <source>Starting to decompress</source>
        <translation>Avvio decompressione</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="132"/>
        <source>is decompressed.</source>
        <translation>è stato decompresso.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="138"/>
        <source>Removed</source>
        <translation>Rimosso</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="151"/>
        <source>Could not delete</source>
        <translation>Impossibile eliminare</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="178"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation>Attenzione! La versione di svtplay-dl che hai scaricato è destinata ai sistemi operativi Linux.
Non funzionerà con il sistema operativo Windows.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="186"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation>Attenzione! La versione di svtplay-dl che hai scaricato è destinata ai sistemi operativi Windows.
Non funzionerà con il sistema operativo Linux.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="219"/>
        <source>Download failed, please try again.</source>
        <translation>Download non riuscito, riprova.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="227"/>
        <location filename="../download.cpp" line="229"/>
        <source>Changed name from</source>
        <translation>Modifica nome da</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="234"/>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation>Per usare questa versione seleziona &quot;svtplay-dl&quot; -&gt; &quot;Usa svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="236"/>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation>Per usare questa versione seleziona &quot;svtplay-dl&quot; -&gt; &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="267"/>
        <source>Failed to decompress.</source>
        <translation>Decompressione fallita.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="257"/>
        <location filename="../downloadunpack.cpp" line="261"/>
        <source>For</source>
        <translation>Per</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="296"/>
        <location filename="../downloadunpack.cpp" line="331"/>
        <location filename="../downloadunpack.cpp" line="404"/>
        <location filename="../downloadunpack.cpp" line="430"/>
        <source>will be downloaded to</source>
        <translation>verrà scaricato in</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="331"/>
        <source>The beta version of svtplay-dl</source>
        <translation>La versione beta di svtplay-dl</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation type="vanished">Per usare questa versione seleziona
&quot;Strumenti&quot; -&gt; &quot;Usa svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="570"/>
        <source>files</source>
        <translation>file</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="572"/>
        <source>file</source>
        <translation>file</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="464"/>
        <location filename="../downloadunpack.cpp" line="489"/>
        <location filename="../filedialog.cpp" line="42"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="131"/>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation>Download svtplay-dl da bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="233"/>
        <source>Current location for stable:</source>
        <translation>Percorso attuale versioen stabile:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="243"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Cattura una schermata (ritardo 5 secondi)</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="365"/>
        <source>svtplay-dl stable version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="331"/>
        <location filename="../downloadunpack.cpp" line="430"/>
        <source>To use this svtplay-dl version, click
&quot;svtplay-dl&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="345"/>
        <location filename="../downloadunpack.cpp" line="404"/>
        <location filename="../downloadunpack.cpp" line="430"/>
        <source>Selected svtplay-dl</source>
        <translation>svtplay-dl selezionato</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="296"/>
        <location filename="../downloadunpack.cpp" line="404"/>
        <source>To use this svtplay-dl version, click
&quot;svtplay-dl&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="233"/>
        <source>Current location for beta:</source>
        <translation>Percorso attuale per beta:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="158"/>
        <source>Select svtplay-dl and download to the &quot;stable&quot; folder and to the &quot;beta&quot; folder.</source>
        <translation>Seleziona svtplay-dl e download nella cartella &quot;stable&quot; e nella cartella &quot;beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="182"/>
        <source>Direct download of stable to the &quot;stable&quot; folder and beta to the &quot;beta&quot; folder.</source>
        <translation>Download diretto della stabile nella cartella &quot;stable&quot; e beta nella cartella &quot;beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="296"/>
        <source>The stable version of svtplay-dl</source>
        <translation>La versione stabile di svtplay-dl</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use stable svtplay-dl&quot;.</source>
        <translation type="vanished">Per usare questa versione di svtplay-dl, fai clic su
&quot;Strumenti&quot;, &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="345"/>
        <location filename="../downloadunpack.cpp" line="365"/>
        <location filename="../downloadunpack.cpp" line="385"/>
        <source>will be downloaded.</source>
        <translation>verrà scaricato.</translation>
    </message>
    <message>
        <source>stable svtplay-dl</source>
        <translation type="vanished">svtplay-dl stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="385"/>
        <source>svtplay-dl beta</source>
        <translation>svtplay-dl beta</translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation type="vanished">Per usare questa versione di svtplay-dl, fai clic su
&quot;Strumenti&quot;, &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="463"/>
        <location filename="../downloadunpack.cpp" line="488"/>
        <location filename="../filedialog.cpp" line="41"/>
        <source>Choose</source>
        <translation>Scegli</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="465"/>
        <location filename="../downloadunpack.cpp" line="490"/>
        <source>Save svtplay-dl in directory</source>
        <translation>Salva svtplay-dl nella cartella</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="38"/>
        <source>The font size changes to the selected font size</source>
        <translation>Dimensione tipo di carattere modificata come da dimensione selezionata</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="44"/>
        <source>Save a screenshot</source>
        <translation>Salva schermata</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="46"/>
        <source>Images (*.png)</source>
        <translation>Immagini (*.png)</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="31"/>
        <source>No error.</source>
        <translation>Nessun errore.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="35"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server remoto ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="39"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Non è stato trovato il nome host remoto (nome host non valido).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="43"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Timeout della connessione verso il server remoto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="47"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>L&apos;operazione è stata annullata tramite chiamate a abort() o close() prima che fosse terminata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="51"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>L&apos;handshake SSL/TLS non è riuscito e non è stato possibile stabilire il canale crittografato.&lt;br&gt;Dovrebbe essere stato emesso il segnale sslErrors().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="55"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete, tuttavia il sistema ha avviato il roaming verso un altro punto di accesso.&lt;br&gt;La richiesta deve essere ripresentata e sarà elaborata non appena la connessione sarà ristabilita.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="59"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete o del mancato avvio della rete.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="63"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>La richiesta in background non è attualmente consentita a causa della politica della piattaforma.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="67"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>È stato raggiunto il limite massimo durante i reindirizzamenti.&lt;br&gt;Per impostazione predefinita, il limite è impostato su 50 o impostato da QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="71"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Durante i reindirizzamenti, l&apos;API di accesso alla rete ha rilevato un reindirizzamento da un protocollo crittografato (https) a uno non crittografato (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="75"/>
        <source>An unknown network-related error was detected.</source>
        <translation>È stato rilevato un errore di rete sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="79"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>La connessione al server proxy è stata rifiutata (il server proxy non accetta richieste).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="83"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server proxy ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="87"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Non è stato trovato il nome host proxy (nome host proxy non valido).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="91"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>La connessione al proxy è scaduta oppure il proxy non ha risposto in tempo alla richiesta inviata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="95"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Per elaborare la richiesta il proxy richiede l&apos;autenticazione ma non ha accettato (se presenti) le credenziali offerte.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="99"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al proxy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="103"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>L&apos;accesso al contenuto remoto è stato negato (simile all&apos;errore HTTP 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="107"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>L&apos;operazione richiesta sul contenuto remoto non è consentita.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="111"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Nel server non è stato trovato il contenuto remoto (simile all&apos;errore HTTP 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="115"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Per elaborare la richiesta il server remoto richiede l&apos;autenticazione ma le credenziali fornite (se presenti) non sono state accettate.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="119"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>La richiesta doveva essere inviata di nuovo, ma questo non è riuscito ad esempio perché non è stato possibile leggere i dati di caricamento una seconda volta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="123"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Impossibile completare la richiesta a causa di un conflitto con lo stato attuale della risorsa.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="127"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>La risorsa richiesta non è più disponibile nel server.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="131"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al contenuto remoto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="135"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>L&apos;API di accesso alla rete non può soddisfare la richiesta perché il protocollo è sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="139"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>L&apos;operazione richiesta non è valida per questo protocollo.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="143"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>È stata rilevata un&apos;interruzione del protocollo (errore di analisi, risposte non valide o impreviste, ecc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="147"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Il server ha riscontrato una condizione imprevista che gli ha impedito di soddisfare la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="151"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Il server non supporta la funzionalità necessaria per soddisfare la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="155"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>In questo momento il server non è in grado di gestire la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="159"/>
        <source>Unknown Server Error.</source>
        <translation>Errore server sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="163"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Il server remoto ha rifiutato la connessione (il server non accetta richieste).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="167"/>
        <source>Unknown Error.</source>
        <translation>Errore sconosciuto.</translation>
    </message>
    <message>
        <location filename="../filedialog.cpp" line="43"/>
        <source>Save </source>
        <translation>Salva </translation>
    </message>
    <message>
        <location filename="../filedialog.cpp" line="43"/>
        <source> in directory</source>
        <translation> nella cartella</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="136"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
</context>
</TS>
