
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadunpack
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/downloadunpack
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include <Qt>
#include <QTimer>
#include <QDir>
#include <QFileDialog>
#include <QSettings>
#include <QToolBar>
#include <QFontDatabase>
#include <QStyleHints>
#include <QStyleFactory>
#include "downloadunpack.h"
#include "qevent.h"
#include "ui_downloadunpack.h"

#if defined (Q_OS_WINDOWS)
DownloadUnpack::DownloadUnpack(bool PYTHON38, QWidget *parent) : QMainWindow(parent), ui(new Ui::DownloadUnpack)
{
    python38 = PYTHON38;
#endif
#if defined (Q_OS_LINUX)
    DownloadUnpack::DownloadUnpack(QWidget * parent) : QMainWindow(parent), ui(new Ui::DownloadUnpack) {
#endif
        Q_INIT_RESOURCE(resource_downloadunpack);
#ifdef Q_OS_LINUX
        QCoreApplication::setAttribute(Qt::AA_DontUseNativeDialogs);
#endif
        // QApplication::setStyle(QStyleFactory::create("Fusion"));
        ui->setupUi(this);
        // setWindowModality(Qt::ApplicationModal);
        ui->pteEdit->setReadOnly(true);
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                           EXECUTABLE_NAME);
#if defined(Q_OS_WINDOWS)
        int id = QFontDatabase::addApplicationFont(":/fonts/segoeui.ttf");
#endif
#if defined(Q_OS_LINUX)
        int id = QFontDatabase::addApplicationFont(":/fonts/Ubuntu-M.ttf");
#endif
        QString family = QFontDatabase::applicationFontFamilies(id).at(0);
        QFont systemfont(family, FONTSIZE);
        QGuiApplication::setFont(systemfont);
#ifdef Q_OS_WINDOWS
        bool dark = (QApplication::palette().window().color().value() < QApplication::palette().windowText().color().value());
#endif

        foreach(QWidget *widget, QApplication::allWidgets()) {
            widget->setFont(QApplication::font());
            widget->update();
        }

        settings.beginGroup("Font");
        QFont currentfon = settings.value(QStringLiteral("currentfont"), QFont()).value<QFont>();
        settings.endGroup();
        ui->pteEdit->setFont(currentfon);
        // Dark Theme
        this->setWindowTitle(tr("Download svtplay-dl from ") + SERVER);
        QIcon icon(":/images/appiconny.png");
        this->setWindowIcon(icon);
        this->addToolBarBreak();
        // Toolbar 1 "Search and Exit"
        QToolBar *toolbar = new QToolBar(tr("Search"), this);
#ifdef Q_OS_WINDOWS
// #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)

        if(dark) {
            toolbar->setStyleSheet("QToolButton::hover {background-color:rgb(45,45,45)} QToolBar {background: rgb(30.30.30)}");
        }

// #endif
#endif
        // }
        //    toolbar->setIconSize(QSize(26, 26));
        toolbar->setIconSize(QSize(22, 22));
        toolbar->setObjectName("toolbar");
        toolbar->setMovable(true);
        toolbar->addAction(ui->actionSearchLatestStable);
        toolbar->addAction(ui->actionSearch);
        toolbar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        toolbar->setVisible(false);
        toolbar->setFont(systemfont);
        //
        // Toobar 2 "Select and download"
        QToolBar *toolbar2 = new QToolBar(tr("Select svtplay-dl and download to the \"stable\" folder and to the \"beta\" folder."), this);
#ifdef Q_OS_WINDOWS
// #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)

        if(dark) {
            toolbar2->setStyleSheet("QToolButton::hover {background-color:rgb(45,45,45)} QToolBar {background: rgb(30.30.30)}");
        }

// #endif
#endif
        toolbar2->setIconSize(QSize(22, 22));
        // Dark Theme
        //    toolbar2->setFont(f);
        toolbar2->setObjectName("toolbar2");
        toolbar2->setMovable(true);
        toolbar2->addAction(ui->actionDownload);
        toolbar2->addAction(ui->actionDownloadToStable);
        toolbar2->addAction(ui->actionDownloadToBleedingedge);
        toolbar2->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        toolbar2->setVisible(false);
        toolbar2->setFont(systemfont);
        //
        //
        // Toolbar 3 "Direct download"
        QToolBar *toolbar3 = new QToolBar(tr("Direct download of stable to the \"stable\" folder and beta to the \"beta\" folder."), this);
#ifdef Q_OS_WINDOWS
// #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)

        if(dark) {
            toolbar3->setStyleSheet("QToolButton::hover {background-color:rgb(45,45,45)} QToolBar {background: rgb(30.30.30)}");
        }

// #endif
#endif
        toolbar3->setIconSize(QSize(22, 22));
        // toolbar3->setPalette(palette);
        // Dark Theme
        //    toolbar3->setFont(f);
        toolbar3->setObjectName("toolbar3");
        toolbar3->setMovable(true);
        toolbar3->addAction(ui->actionDownloadLatestStableToDirectoryStable);
        toolbar3->addAction(ui->actionDownloadLatestBleedingedgeToDirectoryBleedingedge);
        toolbar3->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        toolbar3->addAction(ui->actionExit);
        toolbar3->setFont(systemfont);
        // QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
        //
        //
        this->addToolBar(toolbar);
        this->addToolBarBreak();
        this->addToolBar(toolbar2);
        this->addToolBarBreak();
        this->addToolBar(toolbar3);
        //  this->addToolBarBreak();
        //
        settings.beginGroup("Font");
        // Dark Theme
        QFont font;
        QString strFont = settings.value("currentfont", font.family()).toString();
        int pointsize = settings.value("size", QApplication::font().pointSize()).toInt();
        settings.endGroup();
        QFont font2(strFont);
        font2.setPointSize(pointsize);
        QTextCursor cursor = ui->pteEdit->textCursor();
        ui->pteEdit->selectAll();
        ui->pteEdit->setTextCursor(cursor);
        ui->pteEdit->zoomOut();
        ui->pteEdit->setFont(font2);
        startConfig();
        zoom();
        // File
        // Select location for svtplay-dl stable and bleedingedge
        connect(ui->menuChooseLocation, &QMenu::hovered, [this]() {
            QString stablepath = getStablebetapath("stablepath");
            QString betapath = getStablebetapath("betapath");
            ui->menuChooseLocation->setToolTip(tr("Current location for stable:") + "\n\"" + QDir::toNativeSeparators(stablepath) + "\"\n\n" + tr("Current location for beta:") + "\n\"" + QDir::toNativeSeparators(betapath) + "\"");
            //        ui->statusbar->currentMessage() = tr("Current location:") + "\n\"" + QDir::toNativeSeparators(svtplay_dlpath) + "\"";
        });

        // Context menu
        ui->statusbar->setContextMenuPolicy(Qt::CustomContextMenu);
        QObject::connect(ui->statusbar, &QWidget::customContextMenuRequested, [this]() {
            QAction *screenShot = new QAction;
            QIcon icon(":/images/screenshot.png");
            screenShot->setIcon(icon);
            screenShot->setText(tr("Take a screenshot (5 seconds delay)"));
            QMenu *contextMenu = new QMenu;
            contextMenu->addAction(screenShot);
            QObject::connect(screenShot, &QAction::triggered, this, [this]() {
                QTimer::singleShot(5000, this, &DownloadUnpack::takeAScreenshot);
            });

            contextMenu->exec(QCursor::pos());
            contextMenu->clear();
            contextMenu->deleteLater();
        });

        // SEARCH hovered
        connect(ui->actionSearch, &QAction::hovered, [this]() {
            ui->actionSearch->setToolTip(tr("For") + " " + ui->comboOS->currentText());
        });

        connect(ui->actionSearchLatestStable, &QAction::hovered, [this]() {
            ui->actionSearchLatestStable->setToolTip(tr("For") + " " + ui->comboOS->currentText());
        });

        // DOWNLOAD STABLE TO STABLE
        connect(ui->actionDownloadLatestStableToDirectoryStable, &QAction::triggered, [this]() {
            ui->actionDownloadLatestStableToDirectoryStable->setDisabled(true);
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
            settings.beginGroup("Settings");
            QString latest_stable = settings.value("latest_stable", "noting").toString();
            settings.setValue("latest_stable_downloaded", latest_stable);
            settings.endGroup();
            QString *version = new QString("version_stable.txt");
            QString stablepath = getStablebetapath("stablepath");
            QDir nydir(stablepath + "/stable");

            if(nydir.exists()) {
                nydir.removeRecursively();
            }

            stable_or_snapshot = 1;
            downloadLatest(version, stablepath);
        });

        // DOWNLOAD STABLE TO STABLE HOVERED
        connect(ui->actionDownloadLatestStableToDirectoryStable, &QAction::hovered, [this]() {
            QString stablepath = getStablebetapath("stablepath");
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
            settings.beginGroup("Settings");
            QString latest_stable = settings.value("latest_stable").toString();
            settings.endGroup();
            settings.beginGroup("Path");
            QString svtplay_dlpath = settings.value("stable_bleeding", stablepath).toString();
            settings.endGroup();
            //        ui->actionDownloadLatestStableToDirectoryStable->setStatusTip(" \"" + latest_stable + "\" " + tr("will be downloaded to") + " \"" + QDir::toNativeSeparators(svtplay_dlpath) + "\"");
            ui->actionDownloadLatestStableToDirectoryStable->setToolTip(tr("The stable version of svtplay-dl") + ",\n\"" + latest_stable + "\",\n" + tr("will be downloaded to") + "\n\"" + QDir::toNativeSeparators(svtplay_dlpath + "/stable") + "\"\n" + tr("To use this svtplay-dl version, click\n\"svtplay-dl\", \"Use svtplay-dl stable\"."));
        });

        // DOWNLOAD BLEEDINGEDGE TO BLEEDINGEDGE
        connect(ui->actionDownloadLatestBleedingedgeToDirectoryBleedingedge, &QAction::triggered, [this]() {
            ui->actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setDisabled(true);
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
            settings.beginGroup("Settings");
            QString latest = settings.value("latest", "noting").toString();
            settings.setValue("latest_downloaded", latest);
            settings.endGroup();
            QString *version = new QString("version.txt");
            QString betapath = getStablebetapath("betapath");
            QDir nydir(betapath + "/beta");

            if(nydir.exists()) {
                nydir.removeRecursively();
            }

            stable_or_snapshot = 2;
            downloadLatest(version, betapath);
        });

        // DOWNLOAD BLEEDINGEDGE TO BLEEDINGEDGE HOVERED
        connect(ui->actionDownloadLatestBleedingedgeToDirectoryBleedingedge, &QAction::hovered, [this]() {
            // Download latest?, Latest svtplay-dl-4.10.zip, Path save to
            QString bleedingpath = getStablebetapath("betapath");
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
            settings.beginGroup("Settings");
            QString latest = settings.value("latest").toString();
            settings.endGroup();
            settings.beginGroup("Path");
            QString svtplay_dlpath = settings.value("stable_bleeding", bleedingpath).toString();
            settings.endGroup();
            //        ui->actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setStatusTip(" \"" + stable + "\" " + tr("will be downloaded to") + "\n\"" + QDir::toNativeSeparators(svtplay_dlpath) + "\"");
            ui->actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setToolTip(tr("The beta version of svtplay-dl") + ",\n\"" + latest + "\",\n" + tr("will be downloaded to") + "\n\"" + QDir::toNativeSeparators(svtplay_dlpath + "/beta") + "\"\n" + tr("To use this svtplay-dl version, click\n\"svtplay-dl\", \"Use svtplay-dl beta\"."));
        });

        // Download
        connect(ui->actionDownload, &QAction::triggered, [this]() {
            QString tom;
            tom.clear();
            download(false, tom, tom);
        });

        // Hover download
        connect(ui->actionDownload, &QAction::hovered, [this]() {
            QString currenttext = ui->comboSvtplaydl->currentText();
            ui->actionDownload->setToolTip(tr("Selected svtplay-dl") + ",\n\"" + currenttext + "\",\n" + tr("will be downloaded."));
        });

        // Search latest
        connect(ui->actionSearch, &QAction::triggered, [this]() {
            directoryListing(QString("version"));
        });

        // Latest stable
        connect(ui->actionSearchLatestStable, &QAction::triggered, [this]() {
            directoryListing(QString("version_stable"));
        });

        // DOWNLOAD
        // Latest stable hover
        connect(ui->actionDownloadLatestStable, &QAction::hovered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
            settings.beginGroup("Settings");
            QString latest = settings.value("latest_stable").toString();
            settings.endGroup();
            ui->actionDownloadLatestStable->setToolTip(tr("svtplay-dl stable version") + ",\n\"" + latest + "\",\n" + tr("will be downloaded."));
        });

        // Latest stable Download
        connect(ui->actionDownloadLatestStable, &QAction::triggered, [this]() {
            //        ui->actionDownloadLatestStable->setDisabled(true);
            QString *version = new QString(QStringLiteral("version_stable.txt"));
            downloadLatest(version);
        });

        // latest snapshot hover
        connect(ui->actionDownloadLatest, &QAction::hovered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
            //        settings.beginGroup("Path");
            //        QString path = settings.value("svtplay_dlPath", QStandardPaths::writableLocation(QStandardPaths::DownloadLocation)).toString();
            //        settings.endGroup();
            settings.beginGroup("Settings");
            QString latest = settings.value("latest").toString();
            settings.endGroup();
            //        ui->actionDownloadLatest->setStatusTip(tr("The beta version of svtplay-dl") + ", \"" + latest + "\", " + tr("will be downloaded to") + " \"" + QDir::toNativeSeparators(path) + "\"");
            ui->actionDownloadLatest->setToolTip(tr("svtplay-dl beta") + ",\n\"" + latest + "\",\n" + tr("will be downloaded."));
        });

        // latest snapshot Download
        connect(ui->actionDownloadLatest, &QAction::triggered, [this]() {
            //        ui->actionDownloadLatestStable->setDisabled(true);
            QString *version = new QString(QStringLiteral(u"version.txt"));
            downloadLatest(version);
        });

        connect(ui->actionExit, &QAction::triggered, [ = ]() {
            close();
        });

        /* ------ */
        //    DOWNLOAD TO STABLE
        // Hover
        connect(ui->actionDownloadToStable, &QAction::hovered, [this]() {
            QString currenttext = ui->comboSvtplaydl->currentText();
            ui->actionDownloadToStable->setToolTip(tr("Selected svtplay-dl") + ",\n\"" + currenttext + "\",\n"  + tr("will be downloaded to") + "\n\"" + QDir::toNativeSeparators(getStablebetapath("stablepath") + "/stable") + "\"\n" + tr("To use this svtplay-dl version, click\n\"svtplay-dl\", \"Use svtplay-dl stable\"."));
        });

        // Download to stable
        connect(ui->actionDownloadToStable, &QAction::triggered, [this]() {
            ui->actionDownloadToStable->setDisabled(true);
            QString stablepath = getStablebetapath("stablepath");
            QString currenttext = ui->comboSvtplaydl->currentText();
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
            settings.beginGroup("Settings");
            settings.setValue("latest_stable_downloaded", currenttext);
            settings.endGroup();
            QDir nydir(stablepath + "/stable");

            if(nydir.exists()) {
                nydir.removeRecursively();
            }

            stable_or_snapshot = 1;
            download(false, "", stablepath);
        });

        // Hover bleedingedge
        connect(ui->actionDownloadToBleedingedge, &QAction::hovered, [this]() {
            QString currenttext = ui->comboSvtplaydl->currentText();
            //        ui->actionDownloadToBleedingedge->setStatusTip("\"" + currenttext + "\" " + tr("is downloaded to") + " \"" + QDir::toNativeSeparators(getStablebetapath("betapath")) + "\"");
            ui->actionDownloadToBleedingedge->setToolTip(tr("Selected svtplay-dl") + ",\n\""  + currenttext + "\",\n" + tr("will be downloaded to") + "\n\"" + QDir::toNativeSeparators(getStablebetapath("betapath") + "/beta") + "\"\n" + tr("To use this svtplay-dl version, click\n\"svtplay-dl\", \"Use svtplay-dl beta\"."));
        });

        // Download to bleedingedge
        connect(ui->actionDownloadToBleedingedge, &QAction::triggered, [this]() {
            ui->actionDownloadToBleedingedge->setDisabled(true);
            QString betapath = getStablebetapath("betapath");
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
            QString currenttext = ui->comboSvtplaydl->currentText();
            settings.beginGroup("Settings");
            settings.setValue("latest_downloaded", currenttext);
            settings.endGroup();
            QDir nydir(betapath + "/beta");

            if(nydir.exists()) {
                nydir.removeRecursively();
            }

            stable_or_snapshot = 2;
            download(false, "", betapath);
        });

        /* ---- */
        // Tools
        // DOWNLOAD LOCATION
        connect(ui->actionStable, &QAction::triggered, [this]() {
            //        ui->actionStable->setDisabled(true);
            QString stablepath = getStablebetapath("stablepath");
            /* filedialog */
            QFileDialog filedialog(this);
            int x = this->x();
            int y = this->y();
            filedialog.setDirectory(stablepath);
            filedialog.setLabelText(QFileDialog::Accept, tr("Choose"));
            filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
            filedialog.setWindowTitle(tr("Save svtplay-dl in directory"));
            filedialog.setGeometry(x + 50, y + 50, 900, 550);
            filedialog.setFileMode(QFileDialog::Directory);//Folder name

            if(filedialog.exec() == 1) {
                QDir dir = filedialog.directory();
                QString sdir = dir.path();
                QSettings settings2(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
                settings2.beginGroup("Path");
                settings2.setValue("stablepath", sdir);
                settings2.endGroup();
                settings2.sync();
            }
        });

        connect(ui->actionBleedingedge, &QAction::triggered, [this]() {
            //        ui->actionBleedingedge->setDisabled(true);
            QString betapath = getStablebetapath("betapath");
            /* filedialog */
            QFileDialog filedialog(this);
            int x = this->x();
            int y = this->y();
            filedialog.setDirectory(betapath);
            filedialog.setLabelText(QFileDialog::Accept, tr("Choose"));
            filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
            filedialog.setWindowTitle(tr("Save svtplay-dl in directory"));
            filedialog.setGeometry(x + 50, y + 50, 900, 550);
            filedialog.setFileMode(QFileDialog::Directory);//Folder name

            if(filedialog.exec() == 1) {
                QDir dir = filedialog.directory();
                QString sdir = dir.path();
                QSettings settings2(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
                settings2.beginGroup("Path");
                settings2.setValue("betapath", sdir);
                settings2.endGroup();
                settings2.sync();
            }
        });

        // View
        connect(ui->actionShowToolbar, &QAction::triggered, [ toolbar, toolbar2, toolbar3]() {
            toolbar->setVisible(true);
            toolbar2->setVisible(true);
            toolbar3->setVisible(true);
            toolbar->setAllowedAreas(Qt::TopToolBarArea);
            toolbar2->setAllowedAreas(Qt::TopToolBarArea);
            toolbar3->setAllowedAreas(Qt::TopToolBarArea);
        });

        connect(ui->actionDefaultToolbar, &QAction::triggered, [ toolbar, toolbar2, toolbar3]() {
            toolbar->setVisible(false);
            toolbar2->setVisible(false);
            toolbar3->setVisible(true);
            toolbar->setAllowedAreas(Qt::TopToolBarArea);
            toolbar2->setAllowedAreas(Qt::TopToolBarArea);
            toolbar3->setAllowedAreas(Qt::TopToolBarArea);
        });

        // OS
        connect(ui->comboSvtplaydl, &QComboBox::currentTextChanged, [this]() {
            selectedsvtplaydl = ui->comboSvtplaydl->currentText();
            ui->actionDownload->setEnabled(true);
            //        ui->pteEdit->clear();
            // getLatest(QString("version"));
            // getLatest(QString("version_stable"));
        });
    }

    DownloadUnpack::~DownloadUnpack() {
        delete ui;
    }

    void DownloadUnpack::directoryListing(QString version) {
        getLatest(version);
        QString *test = new QString(version);
        //    ui->pteEdit->clear();
        ui->comboSvtplaydl->clear();
        QUrl url(currentOs() + "list.php");
        auto *nam = new QNetworkAccessManager(nullptr);
        auto *reply = nam->get(QNetworkRequest(url));
        connect(reply, &QNetworkReply::errorOccurred, [this, reply](QNetworkReply::NetworkError error) {
            reply->disconnect();
            // ui->pteEdit->setStyleSheet("background-color:rgb(218,255,221); color:red;");
            ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + "\n" + networkErrorMessages(error));
            setAllEnabled();
            return;
        });

        QObject::connect(
            reply, &QNetworkReply::finished,
        [reply, this, test]() {
            //        QIcon icon(":/images/svtplay-dl.png");
            QByteArray bytes = reply->readAll(); // bytes
            QString listing(bytes);
            listing = listing.trimmed();
            QStringList list = listing.split(',');
            list.removeAll(QString(""));
            list.sort();
            ui->comboSvtplaydl->addItems(list);
            //        ui->comboSvtplaydl->setCurrentIndex(list.size() - 1);
            reply->deleteLater();
            int antalfiler = list.size();

            if(antalfiler > 1) {
                ui->lblAntalFiler->setText(QString::number(antalfiler) + " " + tr("files"));
            } else {
                ui->lblAntalFiler->setText(QString::number(antalfiler) + " " + tr("file"));
            }

            QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
            settings.beginGroup("Settings");
            QString latest;

            if(*test == "version") {
                latest = settings.value("latest", "").toString();
            } else if(*test == "version_stable") {
                latest = settings.value("latest_stable", "").toString();
            }

            settings.endGroup();
            ui->comboSvtplaydl->setCurrentText(latest);
        });
    }

    void DownloadUnpack::klocka() {
        ui->progressBar->setValue(ui->progressBar->value() + 1);

        if(ui->progressBar->value() == 100) {
            ui->progressBar->setValue(0);
        }
    }

    void DownloadUnpack::closeEvent(QCloseEvent * e) {
        endConfig();
        this->deleteLater();
        e->accept();
    }

    void DownloadUnpack::getNewFont(QFont font) {
        ui->pteEdit->setFont(font);
    }
