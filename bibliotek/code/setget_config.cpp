// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadunpack
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/downloadunpack
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QSettings>
#include <QStandardPaths>
#include <QDesktopServices>

#include "downloadunpack.h"

void DownloadUnpack::startConfig()
{
    // Dark Theme
//    QColor c(RED, GREEN, BLUE);
//    QString str = " background-color : " ;
//    str += c.name();
//    ui->pteEdit->setStyleSheet(str);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
    settings.beginGroup("Settings");
    settings.endGroup();
    QIcon linuxIcon(":/images/linuxny.png");
    QIcon winowsIcon(":/images/windowsny.png");
    QSize size(20, 20);
    ui->comboOS->setIconSize(size);
    ui->comboOS->addItem(linuxIcon, "Linux");

    if(QSysInfo::productType() == "windows") {
        if(QSysInfo::currentCpuArchitecture() == "x86_64") {
            ui->comboOS->addItem(winowsIcon, QStringLiteral(u"Windows 64"));
            ui->comboOS->setCurrentIndex(1);
        } else if(QSysInfo::currentCpuArchitecture() == "i386") {
            ui->comboOS->addItem(winowsIcon, QStringLiteral(u"Windows 32"));
            ui->comboOS->setCurrentIndex(1);
        }
    } else {
        ui->comboOS->addItem(winowsIcon, QStringLiteral(u"Windows 64"));
        ui->comboOS->setCurrentIndex(0);
    }

    settings.beginGroup(QStringLiteral(u"MainWindow"));
    this->restoreGeometry(settings.value("savegeometry").toByteArray());
    this->restoreState(settings.value("savestate").toByteArray());
    settings.endGroup();
    connect(ui->actionSourceCode, &QAction::triggered, []() {
        QDesktopServices::openUrl(QUrl(QStringLiteral("https://github.com/spaam/svtplay-dl"), QUrl::TolerantMode));
    });

    connect(ui->actionBinaryFiles, &QAction::triggered, []() {
        QDesktopServices::openUrl(QUrl(QStringLiteral("https://") + SERVER + QStringLiteral("/svtplay-dl"), QUrl::TolerantMode));
    });

//    connect(ui->comboOS, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &DownloadUnpack::directoryListing);
    connect(ui->comboOS, QOverload<int>::of(&QComboBox::currentIndexChanged), [this]() {
        // getLatest(QString("version"));
        // getLatest(QString("version_stable"));
        directoryListing(QString("version"));
    });

    directoryListing(QString("version_stable"));
}

void DownloadUnpack::endConfig()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
    settings.beginGroup("MainWindow");
    settings.setValue("savegeometry", this->saveGeometry());
    settings.setValue("savestate", this->saveState());
    settings.endGroup();
    settings.sync(); // forces to write the settings to storage
}
