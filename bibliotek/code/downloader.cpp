//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Program name
//          Copyright (C) 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "downloader.h"
#include "downloadunpack.h"
#include <QFile>
void Downloader::downloadLatest(const QString &version)
{
    qDebug() << "FITTA 1";
    QUrl url(currentOs() + version);
    QNetworkReply *reply = manager->get(QNetworkRequest(url));
    connect(reply, &QNetworkReply::finished, this, [this, reply]() {
        QByteArray bytes = reply->readAll();
        QString latestVersion = QString(bytes).trimmed();
        QString filename = (m_comboBoxIndex == 0)
                           ? "svtplay-dl-" + latestVersion + ".tar.gz"
                           : "svtplay-dl-" + latestVersion + ".zip";
        emit downloadFinished(filename);
        reply->deleteLater();
    });
}

void Downloader::downloadLatest(const QString &version, const QString &path)
{
    qDebug() << "FITTA 2";
    QUrl url(currentOs() + version);
    QNetworkReply *reply = manager->get(QNetworkRequest(url));
    connect(reply, &QNetworkReply::finished, this, [this, reply, path]() {
        QByteArray bytes = reply->readAll();
        QString latestVersion = QString(bytes).trimmed();
        QString filename = (m_comboBoxIndex == 0)
                           ? "svtplay-dl-" + latestVersion + ".tar.gz"
                           : "svtplay-dl-" + latestVersion + ".zip";
        // Spara nedladdad data till den angivna sökvägen
        QFile file(path + "/" + filename);

        if(file.open(QIODevice::WriteOnly)) {
            file.write(bytes);
            file.close();
        }

        emit downloadFinished(filename);
        reply->deleteLater();
    });
}

void Downloader::getLatest(const QString &version)
{
    qDebug() << "FITTA 3";
    QUrl url(currentOs() + version + ".txt");
    QNetworkReply *reply = manager->get(QNetworkRequest(url));
    connect(reply, &QNetworkReply::finished, this, [this, reply]() {
        qDebug() << "FITTA 4";
        QByteArray bytes = reply->readAll();
        QString latestVersion = QString(bytes).trimmed();
        emit downloadFinished(latestVersion);
        reply->deleteLater();
    });
}

QString Downloader::currentOs()
{
    QString os;

    if(m_comboBoxIndex == 0) {
        os = QStringLiteral("https://") + SERVER + QStringLiteral("/svtplay-dl/Linux/");
    } else if(m_comboBoxIndex == 1) {
#if defined(Q_OS_LINUX)
        os = QStringLiteral("https://") + SERVER + QStringLiteral("/svtplay-dl/Windows/64bit/");
#elif defined(Q_OS_WINDOWS)

        if(python38) {
            if(QSysInfo::currentCpuArchitecture() == "x86_64") {
                os = QStringLiteral("https://") + SERVER + QStringLiteral("/svtplay-dl/Windows/64bit_python3.8/");
            } else  {
                os = QStringLiteral("https://") + SERVER + QStringLiteral("/svtplay-dl/Windows/32bit_python3.8/");
            }
        } else {
            if(QSysInfo::currentCpuArchitecture() == "x86_64") {
                os = QStringLiteral("https://") + SERVER + QStringLiteral("/svtplay-dl/Windows/64bit/");
            } else  {
                os = QStringLiteral("https://") + SERVER + QStringLiteral("/svtplay-dl/Windows/32bit/");
            }
        }

#endif // Q_OS
    }

    return os;
}
