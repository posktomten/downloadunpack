// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          rename
//          Copyright (C) 2022 - 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/downloadunpack
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QDir>
#include <QTimer>
#include "rename.h"

Rename::Rename(QObject *parent)
    : QObject{parent}
{
}

bool Rename::reName(const QString &oldname, const QString &newname)
{
    QTimer::singleShot(1000, [oldname, newname, this]() {
        QDir dir;
        bool check = dir.rename(oldname, newname);

        if(!check) {
            reName(oldname, newname);
        }
    });

    return true;
}
