// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadunpack
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/downloadunpack
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef DOWNLOADUNPACK_H
#define DOWNLOADUNPACK_H

#include <QMainWindow>
#include <QNetworkReply>

#include "ui_downloadunpack.h"


#ifdef Q_OS_WIN
#define CURRENT_YEAR __DATE__
#endif

#define EXECUTABLE_DISPLAY_NAME "streamCapture2"
#define EXECUTABLE_NAME "streamcapture2"
#define LIBRARY_NAME "downloadunpack"
#define SERVER "ceicer.eu"
// #define SERVER "bin.ceicer.com"

#if defined(Q_OS_LINUX) // LINUX
#define FONTSIZE 10
#endif
#if defined(Q_OS_WIN) // LINUX
#define FONTSIZE 10
#endif

QT_BEGIN_NAMESPACE
namespace Ui
{
class DownloadUnpack;
}

QT_END_NAMESPACE

#if defined(Q_OS_LINUX)
#if defined(DOWNLOADUNPACK_LIBRARY)
#include "downloadunpack_global.h"
class DOWNLOADUNPACK_EXPORT DownloadUnpack : public QMainWindow
#else
class DownloadUnpack : public QMainWindow
#endif // DOWNLOADUNPACK_LIBRARY
#else
class DownloadUnpack : public QMainWindow
#endif // Q_OS_LINUX
{
    Q_OBJECT
private:
#if defined (Q_OS_WINDOWS)
    bool python38;
#endif
    int stable_or_snapshot;
    Ui::DownloadUnpack *ui;
    QString selectedsvtplaydl;
    QTimer *timer;
    QString networkErrorMessages(QNetworkReply::NetworkError &error);
    void startConfig();
    QString currentOs();
    void getLatest(const QString version);
    void download(bool downloadlatest, const QString &latest, const QString &path);
    void openFolder();
    void zoom();
    void pteEditTextChanged();
    void zoomDefault();
    void zoomMinus();
    void zoomPlus();
    QString getStablebetapath(QString stable_bleeding);
    void directoryListing(QString version);
    void downloadLatest(QString *version);
    void downloadLatest(QString *version, QString &path);

public:
#if defined (Q_OS_WINDOWS)
    DownloadUnpack(bool PYTHON38, QWidget *parent = nullptr);
#endif
#if defined (Q_OS_LINUX)
    DownloadUnpack(QWidget *parent = nullptr);
#endif

    ~DownloadUnpack();
    void endConfig();
    void closeEvent(QCloseEvent *e);
private slots:
    void klocka();
    QString fileDialog();
    void takeAScreenshot();
    void setAllEnabled();

public slots:
    void getNewFont(QFont font);

signals:
};

#endif // DOWNLOADUNPACK_H
