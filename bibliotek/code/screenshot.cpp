// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadunpack
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/downloadunpack
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#include <QFileDialog>
#include <QSettings>
#include <QScreen>
#include <QStandardPaths>

#include "downloadunpack.h"

void DownloadUnpack::takeAScreenshot()
{
    QScreen *screen = QGuiApplication::primaryScreen();
#ifdef Q_OS_WINDOWS
    QPixmap qpix = screen->grabWindow(0, QApplication::activeWindow()->x(), QApplication::activeWindow()->y(), QApplication::activeWindow()->width() + 20, QApplication::activeWindow()->height() + 70);
#endif
#ifdef Q_OS_LINUX
    QPixmap qpix = screen->grabWindow(0, QApplication::activeWindow()->x(), QApplication::activeWindow()->y(), QApplication::activeWindow()->width(), QApplication::activeWindow()->height() + 30);
#endif
//    QPixmap qpix = screen->grabWindow(0, x - 100, y - 100, this->width() + 200, this->height() + 200);
//    QPixmap qpix = screen->grabWindow(0);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Path");
    QString defaultqpixlocation = settings.value("defaultqpixlocation", QStandardPaths::writableLocation(QStandardPaths::PicturesLocation)).toString();
    settings.endGroup();
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save a screenshot"),
                       defaultqpixlocation,
                       tr("Images (*.png)"));

    if(!fileName.isEmpty()) {
        QFileInfo fi(fileName);
        defaultqpixlocation = fi.path();

        if(fileName.right(4) != ".png") {
            fileName.append(".png");
        }

        qpix.save(fileName);
        settings.beginGroup("Path");
        settings.setValue("defaultqpixlocation", defaultqpixlocation);
        settings.endGroup();
    }
}
