//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Program name
//          Copyright (C) 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "downloadunpack.h"
#include "downloader.h"
void DownloadUnpack::startDownload(const QString &version)
{
    qDebug() << "KUK";
    QThread *thread = new QThread;
    qDebug() << QThread::currentThreadId();
    Downloader *downloader = new Downloader;
    downloader->moveToThread(thread);
    // Anslut signaler och slots
    connect(downloader, &Downloader::requestComboBoxIndex, this, &DownloadUnpack::provideComboBoxIndex);
    connect(this, &DownloadUnpack::sendComboBoxIndex, downloader, &::Downloader::requestComboBoxIndex);
    connect(thread, &QThread::started, [downloader, version]() {
        downloader->downloadLatest(version);
    });

    connect(downloader, &Downloader::downloadFinished, this, &DownloadUnpack::onDownloadFinished);
    connect(downloader, &Downloader::progress, this, &DownloadUnpack::onDownloadProgress);
    connect(thread, &QThread::finished, thread, &QThread::deleteLater);
    connect(thread, &QThread::finished, downloader, &Downloader::deleteLater);
    thread->start();
}

void DownloadUnpack::onDownloadFinished(const QString &filename)
{
    // Uppdatera GUI med nedladdningsresultatet
    ui->pteEdit->setText(tr("Downloaded file: %1").arg(filename));
}

void DownloadUnpack::onDownloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    ui->progressBar->setMaximum(bytesTotal);
    ui->progressBar->setValue(bytesReceived);
}
