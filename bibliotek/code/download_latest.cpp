// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadunpack
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/downloadunpack
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QSettings>
#include "downloadunpack.h"


void DownloadUnpack::downloadLatest(QString *version)
{
    // ui->pteEdit->clear();
    // ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + tr("Preparing to download...") + '\n' + tr("It may take a while...") + '\n');
    stable_or_snapshot = 0;
    /*     */
    QByteArray *bytes = new QByteArray;
    QUrl url(currentOs() + *version);
    auto *nam = new QNetworkAccessManager(nullptr);
    nam->deleteLater();
    auto *reply = nam->get(QNetworkRequest(url));
    reply->waitForBytesWritten(1000);
    QObject::connect(
        reply, &QNetworkReply::finished,
    [this, reply, bytes]() {
        *bytes = reply->readAll(); // bytes
        QString latestVersion = (QString) * bytes;
        latestVersion = latestVersion.trimmed();
        QString ut;

        if(ui->comboOS->currentIndex() == 0) {
            ut = "svtplay-dl-" + latestVersion + ".tar.gz";
        } else  {
            ut = "svtplay-dl-" + latestVersion + ".zip";
        }

        delete reply;
        download(true, ut, "");
    });

    /*    */
}



void DownloadUnpack::downloadLatest(QString *version, QString &path)
{
    ui->pteEdit->clear();
    ui->pteEdit->setPlainText(ui->pteEdit->toPlainText() + tr("Preparing to download...") + '\n' + tr("It may take a while...") + '\n');
    QString *test = new QString(path);
    /*     */
    QByteArray *bytes = new QByteArray;
    QUrl url(currentOs() + *version);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    reply->waitForBytesWritten(1000);
    QObject::connect(
        reply, &QNetworkReply::finished,
    [this, reply, bytes, test]() {
        *bytes = reply->readAll(); // bytes
        QString latestVersion = (QString) * bytes;
        latestVersion = latestVersion.trimmed();
        QString ut;

        if(ui->comboOS->currentIndex() == 0) {
            ut = "svtplay-dl-" + latestVersion + ".tar.gz";
        } else  {
            ut = "svtplay-dl-" + latestVersion + ".zip";
        }

        delete reply;
        download(true, ut, *test);
    });

    /*    */
}

void DownloadUnpack::getLatest(const QString version)
{
    QString *test = new QString;
    *test = version;
    /*     */
    QByteArray *bytes = new QByteArray;
    QUrl url(currentOs() + version + ".txt");
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    reply->waitForBytesWritten(1000);
    QObject::connect(
        reply, &QNetworkReply::finished,
    [this, reply, bytes, test]() {
        *bytes = reply->readAll(); // bytes
        QString latestVersion = (QString) * bytes;
        latestVersion = latestVersion.trimmed();
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME, LIBRARY_NAME);
        settings.beginGroup("Settings");
        QString ut;

        if(ui->comboOS->currentIndex() == 0) {
            ut = "svtplay-dl-" + latestVersion + ".tar.gz";
        } else if(ui->comboOS->currentIndex() == 1)   {
            ut = "svtplay-dl-" + latestVersion + ".zip";
        }

        if(*test == "version") {
            settings.setValue("latest", ut);
        } else if(*test == "version_stable") {
            settings.setValue("latest_stable", ut);
        }

        settings.endGroup();
        settings.sync();
        //        download(true, ut, "");
    });

    /*    */
}
