// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadunpack
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QSettings>
#include "downloadunpack.h"

void DownloadUnpack::zoom()
{
    connect(ui->actionZoomDefault, &QAction::triggered, this, &DownloadUnpack::zoomDefault);
    connect(ui->actionZoomOut, &QAction::triggered,  this, &DownloadUnpack::zoomMinus);
    connect(ui->actionZoomIn, &QAction::triggered, this, &DownloadUnpack::zoomPlus);
    connect(ui->pteEdit, &QTextEdit::textChanged, this, &DownloadUnpack::pteEditTextChanged);
    connect(ui->actionZoomDefault, &QAction::hovered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        QFont f = QApplication::font();
        int default_pointsize = f.pointSize();
        settings.beginGroup("Font");
        int pointsize = settings.value("size", default_pointsize).toInt();
        settings.endGroup();
        ui->actionZoomDefault->setStatusTip(tr("The font size changes to the selected font size") + " (" + QString::number(pointsize) + ").");
    });
}

void DownloadUnpack::pteEditTextChanged()
{
    QTextCursor cursor = ui->pteEdit->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->pteEdit->setTextCursor(cursor);
}

void DownloadUnpack::zoomDefault()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Font");
    QFont f = QApplication::font();
    int default_pointsize = f.pointSize();
    int pointsize = settings.value("size", default_pointsize).toInt();
    settings.endGroup();
    QFont fdisplay = ui->pteEdit->font();
    fdisplay.setPointSize(pointsize);
    ui->pteEdit->setFont(fdisplay);
    QTextCursor cursor = ui->pteEdit->textCursor();
    ui->pteEdit->selectAll();
    ui->pteEdit->setTextCursor(cursor);
    // ui->pteEdit->zoomOut();
}

void DownloadUnpack::zoomMinus()
{
    QFont f = ui->pteEdit->font();
    int pointsize = f.pointSize();

    if(pointsize > 6) {
        QTextCursor cursor = ui->pteEdit->textCursor();
        ui->pteEdit->selectAll();
        ui->pteEdit->setFont(f);
        f.setPointSize(f.pointSize() - 1);
        ui->pteEdit->setTextCursor(cursor);
        ui->pteEdit->zoomOut(1);
    }
}

void DownloadUnpack::zoomPlus()
{
    QFont f = ui->pteEdit->font();
    int pointsize = f.pointSize();

    if(pointsize < 24) {
        QTextCursor cursor = ui->pteEdit->textCursor();
        ui->pteEdit->selectAll();
        ui->pteEdit->setFont(f);
        f.setPointSize(f.pointSize() + 1);
        ui->pteEdit->setTextCursor(cursor);
        ui->pteEdit->zoomIn(1);
    }
}

//void DownloadUnpack::wheelEvent(QWheelEvent *ev)
//{
//    if(QApplication::queryKeyboardModifiers() == Qt::ControlModifier) {
//        QPoint numDegrees = ev->angleDelta();
//        int direction = numDegrees.y();

//        if(direction < 0) {
//            zoomMinus();
//        } else if(direction > 0) {
//            zoomPlus();
//        }

//        ev->accept();
//    }
//}
