

set "QTVERSION=5.15.16"
set PATH=C:\Qt\%QTVERSION%\mingw810_32\bin;C:\Qt\Tools\mingw810_32\bin;%PATH%
::set PATH=C:\Qt\%QTVERSION%\mingw810_32\bin;C:\Qt\Tools\mingw32-gcc-13.1.0\bin;%PATH%

mkdir build
cd build
    qmake.exe -project ../code/*.pro
	qmake.exe ../code/*.pro -spec win32-g++ "CONFIG+=qtquickcompiler" && mingw32-make.exe qmake_all
  
mingw32-make.exe -j6
mingw32-make.exe clean -j6
cd ..
rmdir /S /Q build
mkdir lib_Qt-%QTVERSION%_MinGW_32
copy build-lib\*.* lib_Qt-%QTVERSION%_MinGW_32
rmdir /S /Q build-lib



