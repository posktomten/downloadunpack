setlocal enabledelayedexpansion

set "QT6=6.8.1"
set "MINGW=llvm-mingw_64"
set "GCC=llvm-mingw1706_64"
set "CURRENT_PATH=%cd%"
set "DIR=build"



echo Your current path: "%CURRENT_PATH%"
echo Build directory: "%DIR%"

set "PATH=C:\Qt\%QT6%\%MINGW%\bin;%PATH%"
set "PATH=C:\Qt\Tools\%GCC%\bin;%PATH%"
set "PATH=C:\Program Files\CMake\bin;%PATH%"
set "PATH=C:\Program Files\Git\bin;%PATH%"

echo.
cmake -S "%CURRENT_PATH%\code" -B "%CURRENT_PATH%\%DIR%" -G "Ninja" -DCMAKE_BUILD_TYPE=Debug

cmake --build "%CURRENT_PATH%\%DIR%" --target all

mkdir %CURRENT_PATH%\lib_Qt-%QT6%_MinGW_LLVM
xcopy %CURRENT_PATH%\%DIR%-lib\* %CURRENT_PATH%\lib_Qt-%QT6%_MinGW_LLVM

rmdir /s /q "%CURRENT_PATH%\%DIR%-lib"

echo.
if not exist "%CURRENT_PATH%\%DIR%-lib" (
    echo "%CURRENT_PATH%\%DIR%" is deleted
) else (
    echo Unable to delete "%CURRENT_PATH%\%DIR%-dir"
)

echo.
cmake -S "%CURRENT_PATH%\code" -B "%CURRENT_PATH%\%DIR%" -G "Ninja" -DCMAKE_BUILD_TYPE=Release

cmake --build "%CURRENT_PATH%\%DIR%" --target all


xcopy %CURRENT_PATH%\%DIR%-lib\* %CURRENT_PATH%\lib_Qt-%QT6%_MinGW_LLVM

rmdir /s /q "%CURRENT_PATH%\%DIR%-lib"
rmdir /s /q "%CURRENT_PATH%\%DIR%"

echo.
if not exist "%CURRENT_PATH%\%DIR%" (
    echo "%CURRENT_PATH%\%DIR%-lib" is deleted
) else (
    echo Unable to delete "%CURRENT_PATH%\%DIR%-lib"
)

endlocal
