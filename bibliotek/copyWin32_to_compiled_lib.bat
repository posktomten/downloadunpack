
set "SOKVAG=C:\Users\posktomten\PROGRAMMERING\compiled_lib"
set "QTVERSION=5.15.16"
set "MSVC=MSVC2022_32"
set "MINGV=MinGW_32"

::mkdir %SOKVAG%\lib_Qt5.15.2
mkdir %SOKVAG%\lib_Qt-%QTVERSION%_%MINGV%
mkdir %SOKVAG%\lib_Qt-%QTVERSION%_%MSVC%
mkdir %SOKVAG%\lib_Qt-5.6.2_%MINGV%

::xcopy /y lib_Qt5.15.2\*.* %SOKVAG%\lib_Qt5.15.2\
xcopy /y lib_Qt-%QTVERSION%_%MINGV%\*.* %SOKVAG%\lib_Qt-%QTVERSION%_%MINGV%\
xcopy /y lib_Qt-5.6.2_%MINGV%\*.* %SOKVAG%\lib_Qt-5.6.2_%MINGV%\
xcopy /y lib_Qt-%QTVERSION%_%MSVC%\*.* %SOKVAG%\lib_Qt-%QTVERSION%_%MSVC%\
