@echo off

call "C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
set QTVERSIONS=5.15.2 5.15.16
set PROFILE="downloadunpack"

for %%Q in (%QTVERSIONS%) do (
    setlocal enabledelayedexpansion

    set _QMAKE=C:\Qt\%%Q\MSVC2019_64\bin\qmake.exe

    if %%Q NEQ "5.15.16" (

        mkdir build
        cd build
	
        !_QMAKE! -project ../code/%PROFILE%.pro 
        !_QMAKE! "CONFIG+=debug"  ../code/%PROFILE%.pro -spec win32-msvc 
        nmake.exe

        cd ..
        rmdir /S /Q build
        mkdir lib_Qt-%%Q_MSVC2022
        copy /Y build-lib\* lib_Qt-%%Q_MSVC2022\
        rmdir /S /Q build-lib
	) else (
        mkdir lib_Qt-%%Q_MSVC2022
    )

    mkdir build
    cd build
	
   !_QMAKE! -project ../code/%PROFILE%.pro 
   !_QMAKE! "CONFIG+=release"  ../code/%PROFILE%.pro -spec win32-msvc 
    nmake.exe

    cd ..
    rmdir /S /Q build
    copy /Y build-lib\* lib_Qt-%%Q_MSVC2022\
    rmdir /S /Q build-lib

   endlocal
)
