::@echo off
set QTVERSIONS_5=5.15.2 5.15.16

set "PATH=C:\Qt\Tools\mingw1310_64\bin;%PATH%"


for %%Q in (%QTVERSIONS_5%) do (
    setlocal enabledelayedexpansion
	
    set _QMAKE=C:\Qt\%%Q\mingw81_64\bin\qmake.exe

    if %%Q NEQ 5.15.16 (

        mkdir build
        cd build
	 
   !_QMAKE! -project ../code/*.pro 
   !_QMAKE! -spec win32-g++ "CONFIG+=debug" "CONFIG+=qml_debug"  ../code/*.pro && %COMPILER% qmake_all
   mingw32-make.exe -j6
   mingw32-make.exe clean

        cd ..
        rmdir /S /Q build
        mkdir lib_Qt-%%Q_MinGW
        copy /Y build-lib\* lib_Qt-%%Q_MinGW\
        rmdir /S /Q build-lib
	) else (
        mkdir lib_Qt-%%Q_MinGW
    )

    mkdir build
    cd build
	
   !_QMAKE! -project ../code/*.pro 
   !_QMAKE! -spec win32-g++ "CONFIG+=release" "CONFIG+=qml_release"  ../code/*.pro && %COMPILER% qmake_all
   mingw32-make.exe -j6
   mingw32-make.exe clean

   


    cd ..
    rmdir /S /Q build
    copy /Y build-lib\* lib_Qt-%%Q_MinGW\
    rmdir /S /Q build-lib

   endlocal
)



