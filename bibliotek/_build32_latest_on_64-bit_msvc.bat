

call "C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat" x86
set PROFILE="downloadunpack"
set QTVERSION="5.15.16"
set QMAKE="C:\Qt\%QTVERSION%\MSVC2019_32\bin\qmake.exe"
::set PATH="C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Tools\MSVC\14.41.34120\bin\Hostx64\x86";%PATH%

::mkdir build
::cd build

    ::%QMAKE% -project ../code/%PROFILE%.pro 
	::%QMAKE% "CONFIG+=release" ../code/%PROFILE%.pro -o Makefile -spec win32-msvc "CONFIG+=qtquickcompiler" "CONFIG+=debug"
	::set CL="/MP"
    ::nmake.exe
	
::cd ..
::rmdir /S /Q build
::mkdir lib_Qt%QTVERSION%_MSVC2022
::copy /Y build-lib\* lib_Qt%QTVERSION%_MSVC2022\
::rmdir /S /Q build-lib



mkdir build
cd build

    %QMAKE% -project ../code/%PROFILE%.pro 
	%QMAKE% "CONFIG+=debug" ../code/%PROFILE%.pro -o Makefile -spec win32-msvc "CONFIG+=qtquickcompiler" "CONFIG+=release"
    nmake.exe
	
cd ..
mkdir lib_Qt-%QTVERSION%_MSVC2022_32
rmdir /S /Q build
copy /Y build-lib\* lib_Qt-%QTVERSION%_MSVC2022_32\
rmdir /S /Q build-lib