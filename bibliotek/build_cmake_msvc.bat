@echo off
setlocal enabledelayedexpansion
call "C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
set "QT6=6.8.1_static"
set "MSVC=msvc2022_64"

set "CURRENT_PATH=%cd%"
set "DIR=build"

lib_Qt-6.8.1_static_MSVC2022

echo Your current path: "%CURRENT_PATH%"
echo Build directory: "%DIR%"

set "PATH=C:\Qt\%QT6%\%MSVC%\bin;%PATH%"
set "PATH=C:\Qt\Tools\Ninja;%PATH%"
set "PATH=C:\Program Files\CMake\bin;%PATH%"
set "PATH=C:\Program Files\Git\bin;%PATH%"

echo.
cmake -S "%CURRENT_PATH%\code" -B "%CURRENT_PATH%\%DIR%" -G "Ninja" -DCMAKE_BUILD_TYPE=Debug

cmake --build "%CURRENT_PATH%\%DIR%" --target all

mkdir %CURRENT_PATH%\lib_Qt-%QT6%_MSVC2022
xcopy %CURRENT_PATH%\%DIR%-lib\* %CURRENT_PATH%\lib_Qt-%QT6%_MSVC2022

rmdir /s /q "%CURRENT_PATH%\%DIR%-lib"
rmdir /s /q "%CURRENT_PATH%\%DIR%"

echo.
if not exist "%CURRENT_PATH%\%DIR%" (
    echo "%CURRENT_PATH%\%DIR%" is deleted
) else (
    echo Unable to delete "%CURRENT_PATH%\%DIR%"
)


echo.
cmake -S "%CURRENT_PATH%\code" -B "%CURRENT_PATH%\%DIR%" -G "Ninja" -DCMAKE_BUILD_TYPE=Release

cmake --build "%CURRENT_PATH%\%DIR%" --target all

xcopy %CURRENT_PATH%\%DIR%-lib\* %CURRENT_PATH%\lib_Qt-%QT6%_MSVC2022

rmdir /s /q "%CURRENT_PATH%\%DIR%-lib"
rmdir /s /q "%CURRENT_PATH%\%DIR%"

echo.
if not exist "%CURRENT_PATH%\%DIR%" (
    echo "%CURRENT_PATH%\%DIR%-lib" is deleted
) else (
    echo Unable to delete "%CURRENT_PATH%\%DIR%-lib"
)

endlocal
